-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 11, 2021 at 04:04 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Tores_Web`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `email`, `no_telp`, `password`) VALUES
(1, 'Diaz Nugraha', 'diaz@tores.com', '087732802699', 'd36a36c25ffe288126d40000c6c5714e');

-- --------------------------------------------------------

--
-- Table structure for table `baju`
--

CREATE TABLE `baju` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `jumlah` int(3) NOT NULL,
  `harga` varchar(20) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `baju`
--

INSERT INTO `baju` (`id`, `nama`, `jenis`, `jumlah`, `harga`, `gambar`) VALUES
(4, 'Baju Jas 1', 'Baju Jas', 8, '100000', 'jas1.jpeg'),
(6, 'Baju Jas 2', 'Baju Jas', 20, '200000', 'jas2.jpeg'),
(7, 'Baju Kebaya 1', 'Baju Kebaya', 14, '200000', 'kebaya1.jpeg'),
(8, 'Baju Kebaya 2', 'Baju Kebaya', 10, '100000', 'kebaya2.jpeg'),
(9, 'Baju Kebaya 3', 'Baju Kebaya', 18, '100000', 'kebaya3.jpeg'),
(10, 'Baju Pengantin 1', 'Baju Pengantin', 17, '100000', 'baju_pengantin1.jpeg'),
(11, 'Baju Pengantin 2', 'Baju Pengantin', 12, '100000', 'baju_pengantin2.jpeg'),
(12, 'Baju Pengantin 3', 'Baju Pengantin', 15, '200000', 'baju_pengantin3.jpeg'),
(16, 'Baju Kemeja 1', 'Baju Kemeja', 12, '100000', 'kemeja1.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `check_out`
--

CREATE TABLE `check_out` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `harga` varchar(10) NOT NULL,
  `tanggal_booking` varchar(20) NOT NULL,
  `tanggal_pinjam` varchar(20) NOT NULL,
  `id_baju` text NOT NULL,
  `jumlah` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `check_out`
--

INSERT INTO `check_out` (`id`, `nama`, `alamat`, `harga`, `tanggal_booking`, `tanggal_pinjam`, `id_baju`, `jumlah`, `id_user`, `no_telp`, `email`, `status`) VALUES
(29, 'Diaz Nugraha', 'polindra', '300000', '2020-12-30', '2020-12-31', '4, 12', '1, 1', 14, '087732802699', 'diaz@gmail.com', '2'),
(30, 'Diaz Nugraha', 'polindra', '400000', '2020-12-31', '2021-01-18', '6, 11, 8', '1, 1, 1', 14, '087732802699', 'diaz@gmail.com', '1'),
(31, 'Diaz Nugraha', 'polindra lagi', '300000', '2020-12-31', '2021-01-27', '4, 12', '1, 1', 14, '087732802699', 'diaz@gmail.com', '1'),
(33, 'Diaz Nugraha', 'polindra', '100000', '2020-12-31', '2020-12-13', '4', '1', 14, '087732802699', 'diaz@gmail.com', '2'),
(34, 'Diaz Nugraha', 'polindra', '400000', '2020-12-31', '2021-01-18', '4, 8', '1, 1', 14, '087732802699', 'diaz@gmail.com', '1'),
(35, 'Iqbal Rasetio', 'polindra', '400000', '2020-12-31', '2021-01-11', '4, 12, 10', '1, 1, 1', 17, '0813-2462-6295', 'iqbal@gmail.com', '1'),
(36, 'Nopiya Sari', 'polindra', '500000', '2020-12-31', '2021-01-26', '7, 10, 12', '1, 1, 1', 18, '083837929821', 'nopiya@gmail.com', '1'),
(37, 'Mohamad Safik Seftian', 'polindra', '1000000', '2021-01-01', '2021-01-18', '4, 6, 7', '1, 1, 1', 19, '081280974203', 'safik@gmail.com', '1'),
(38, 'Diaz Nugraha', 'Polindra', '200000', '2021-01-03', '2021-01-18', '4', '2', 14, '087732802699', 'diaz@gmail.com', '1');

-- --------------------------------------------------------

--
-- Table structure for table `salon`
--

CREATE TABLE `salon` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salon`
--

INSERT INTO `salon` (`id`, `nama`, `jenis`, `tempat`, `gambar`) VALUES
(1, 'Model Rambut 1', 'Potong Rambut', 'Toko', 'model_rambut1.jpeg'),
(2, 'Model Rambut 2', 'Potong Rambut', 'Toko', 'model_rambut2.jpeg'),
(3, 'Model Rambut 3', 'Potong Rambut', 'Toko', 'model_rambut3.jpeg'),
(4, 'Model Rambut 4', 'Potong Rambut', 'Toko', 'model_rambut4.jpeg'),
(5, 'Creambath 1', 'Creambath Station', 'Toko', 'creambath1.jpeg'),
(6, 'Creambath 2', 'Creambath Station', 'Toko', 'creambath2.jpeg'),
(7, 'Creambath 3', 'Creambath Station', 'Toko', 'creambath3.jpeg'),
(8, 'Potong Rambut 1', 'Potong Rambut', 'Toko', 'Potong_rambut1.jpeg'),
(9, 'Potong Rambut 2', 'Potong Rambut', 'Toko', 'Potong_rambut2.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `tata_rias`
--

CREATE TABLE `tata_rias` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tata_rias`
--

INSERT INTO `tata_rias` (`id`, `nama`, `jenis`, `status`, `gambar`) VALUES
(1, 'Tata Rias Pengantin 1', 'Tata Rias Pengantin', 'tersedia', 'tata_rias_pengantin1.jpeg'),
(2, 'Tata Rias Pengantin 2', 'Tata Rias Pengantin', 'tersedia', 'tata_rias_pengantin2.jpeg'),
(3, 'Tata Rias Pengantin 3', 'Tata Rias Pengantin', 'tersedia', 'tata_rias_pengantin3.jpeg'),
(4, 'Tata Rias Khitanan 1', 'Tata Rias Khitanan', 'tersedia', 'tata_rias_khitanan1.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `toko`
--

CREATE TABLE `toko` (
  `id` int(11) NOT NULL,
  `pendapatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `toko`
--

INSERT INTO `toko` (`id`, `pendapatan`) VALUES
(1, 3500000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `no_telp`, `password`) VALUES
(14, 'Diaz Nugraha', 'diaz@gmail.com', '087732802699', 'd36a36c25ffe288126d40000c6c5714e'),
(17, 'Iqbal Rasetio', 'iqbal@gmail.com', '081324626295', 'eedae20fc3c7a6e9c5b1102098771c70'),
(18, 'Nopiya Sari', 'nopiya@gmail.com', '083837929821', '3cd34d749a4f4e2a8922397e3621ec1a'),
(19, 'Mohamad Safik Seftian', 'safik@gmail.com', '081280974203', 'aff09aa2b69af792b5cfb285c522c482');

-- --------------------------------------------------------

--
-- Table structure for table `user_cart`
--

CREATE TABLE `user_cart` (
  `id` int(11) NOT NULL,
  `id_user` varchar(20) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `jumlah` int(2) NOT NULL,
  `harga` varchar(10) NOT NULL,
  `hargaUpdate` varchar(10) NOT NULL,
  `gambar` text NOT NULL,
  `id_baju` int(11) NOT NULL,
  `jumlah_baju` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_cart`
--

INSERT INTO `user_cart` (`id`, `id_user`, `nama`, `jumlah`, `harga`, `hargaUpdate`, `gambar`, `id_baju`, `jumlah_baju`) VALUES
(161, '14', 'Baju Jas 1', 2, '100000', '200000', 'jas1.jpeg', 4, 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `baju`
--
ALTER TABLE `baju`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `check_out`
--
ALTER TABLE `check_out`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salon`
--
ALTER TABLE `salon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tata_rias`
--
ALTER TABLE `tata_rias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toko`
--
ALTER TABLE `toko`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user_cart`
--
ALTER TABLE `user_cart`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `baju`
--
ALTER TABLE `baju`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `check_out`
--
ALTER TABLE `check_out`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `salon`
--
ALTER TABLE `salon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tata_rias`
--
ALTER TABLE `tata_rias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `toko`
--
ALTER TABLE `toko`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_cart`
--
ALTER TABLE `user_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
