
 
   
    <div class="jumbotron jumbotron-fluid">
      <!-- Flasher -->
    <div class="row">
      <div class="col-lg-6">
        <?php Flasher::loginFlash(); ?>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <?php Flasher::registrasiFlash(); ?>
      </div>
    </div>
    <!-- /Flasher -->
          <div class="container">
              <a href="<?= BASEURL; ?>/galeri" class="btn tombol-2">See More</a>
              <h1 class="display-6">Penyedia layanan tata rias yang lengkap dan professional</h1>
            </div>
          </div>
          <!-- container -->
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-10 info-panel">
                <div class="row">
                  <div class="col-lg">
                    <img src="<?= BASEURL; ?>/img/employee.png" alt="Employee" class="float-left">
                    <h4>Layanan 24 jam</h4>
                    <p>Hubungi kami : 087732802699</p>
                  </div>
                  <div class="col-lg">
                    <img src="<?= BASEURL; ?>/img/hires.png" alt="Employee" class="float-left">
                    <h4>Foto Produk</h4>
                    <p>Untuk melihat foto dari produk</p>
                  </div>
                  <div class="col-lg">
                    <img src="<?= BASEURL; ?>/img/security.png" alt="Employee" class="float-left">
                    <h4>Keamanan</h4>
                    <p>Keamanan dan kenyamanan menggunakan jasa kami</p>
                  </div>
                </div>
              </div>
            </div>

            <!-- Workspace  -->
            <div class="row workingspace">
              <div class="col-lg-6">
                <img src="img/gambar1.jpg" alt="workingspace" class="img-fluid">
              </div>
              <div class="col-lg-5">
                <h3>Tores salon dan tata rias</h3>
                <p>Penyedia layanan tata rias yang lengkap, professional dengan mengutamakan kenyamanan dan kepuasan pelanggan</p>
                <a href="<?= BASEURL; ?>/galeri" class="btn tombol">Produk</a>
              </div>
            </div>
          </div>
          <br>
          <br>
          <!-- Akhir container -->
        <script src="<?= BASEURL; ?>/js/jquery.js"></script>
        <script src="<?= BASEURL; ?>/js/bootstrap.js"></script>
    </body>
</html>