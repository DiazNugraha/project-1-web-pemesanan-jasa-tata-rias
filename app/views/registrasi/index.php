
    <div class="col-md-4 col-md-offset-4 form-login">

        <div class="outter-form-login">
            <div class="logo-login">
                <img src="<?= BASEURL; ?>/img/user-alt.png" alt="" width="36"> 
            </div>
            <h3 class="text-center title-login">Registrasi</h3>
            <form action="<?= BASEURL; ?>/registrasi/tambahUser" class="form-horizontal" method="post">

                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" name="username" id="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email"id="email">
                        </div>
                        <div class="form-group">
                            <label for="notelp">Nomor Telepon</label>
                            <input type="text" class="form-control" name="notelp" id="notelp">
                        </div>
                        <!-- <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" class="form-control" name="alamat" id="alamat">
                        </div> -->
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>

                        <div class="form-group">
                            <label for="repassword">Ketik ulang password</label>
                            <input type="password" class="form-control" name="repassword" id="repassword">
                        </div>

                        <input type="submit" class="btn btn-block btn-custom-green" value="REGISTER" />
                        <div class="text-center forget">
                        <p>Back To <a href="<?= BASEURL; ?>/login">Login</a></p>
                    </div>

                </div>
               
                
               
            </form>
        </div>
    </div>
