
 
   
    <div class="jumbotron jumbotron-fluid">
      <!-- Flasher -->
    <div class="row">
      <div class="col-lg-6">
        <?php Flasher::loginFlash(); ?>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        <?php Flasher::registrasiFlash(); ?>
      </div>
    </div>
    <!-- /Flasher -->
          <div class="container">
              <a href="" class="btn tombol-2">See More</a>
              <h1 class="display-6">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quaerat, officiis.</h1>
            </div>
          </div>
          <!-- container -->
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-10 info-panel">
                <div class="row">
                  <div class="col-lg">
                    <img src="<?= BASEURL; ?>/img/employee.png" alt="Employee" class="float-left">
                    <h4>24 Hours</h4>
                    <p>Lorem ipsum dolor sit amet.</p>
                  </div>
                  <div class="col-lg">
                    <img src="<?= BASEURL; ?>/img/hires.png" alt="Employee" class="float-left">
                    <h4>24 Hours</h4>
                    <p>Lorem ipsum dolor sit amet.</p>
                  </div>
                  <div class="col-lg">
                    <img src="<?= BASEURL; ?>/img/security.png" alt="Employee" class="float-left">
                    <h4>24 Hours</h4>
                    <p>Lorem ipsum dolor sit amet.</p>
                  </div>
                </div>
              </div>
            </div>

            <!-- Workspace  -->
            <div class="row workingspace">
              <div class="col-lg-6">
                <img src="img/workingspace.png" alt="workingspace" class="img-fluid">
              </div>
              <div class="col-lg-5">
                <h3>You <span>work</span> like at <span>home</span></h3>
                <p>bekerja dengan suasana hati yang lebih asik dan mempelajari hal baru setiap harinya.</p>
                <a href="" class="btn tombol">Gallery</a>
              </div>
            </div>
          </div>
          <br>
          <br>
          <!-- Akhir container -->
        <script src="<?= BASEURL; ?>/js/jquery.js"></script>
        <script src="<?= BASEURL; ?>/js/bootstrap.js"></script>
    </body>
</html>