
                
                    <div class="row ml-4">
                
                        <!-- Area Chart -->
                        <div class="col-xl-11 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Details</h6>
                                    <div class="dropdown no-arrow">
                                       
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                
                                    <div class="row mt-5 ml-md-3">
                                    <div class="card-gambar">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                        <!-- <svg class="bd-placeholder-img" width="100%" height="400" xmlns="http://www.w3.org/2000/svg" aria-label="Placeholder: Image" preserveAspectRatio="xMidYMid slice" role="img"><title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"/><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image</text></svg> -->
                                        <img src="<?= BASEURL ?>/img/barang/<?= $data['barang']['gambar']; ?>" alt="">

                                        </div>
                                        <div class="col-md-7">
                                        <div class="card-body">
                                            <h5 class="card-title"><?= $data['barang']['nama']; ?></h5>
                                            <br>
                                            <!-- <p class="card-text">It's a broader card with text below as a natural lead-in to extra content. This content is a little longer.</p> -->
                                            <p class="card-text">Nama &emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&emsp;&emsp;: <?= $data['barang']['nama']; ?></p>
                                            <p class="card-text">Jenis &emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;: <?= $data['barang']['jenis']; ?></p>
                                            <p class="card-text">Merk &emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;: <?= $data['barang']['merk']; ?></p>
                                            <p class="card-text">Jumlah Tersedia&emsp;&emsp; : <?= $data['barang']['jumlah']; ?></p>
                                            
                                            <br>
                                            <br>
                                            <br>
                                            <a href="" type="button" class="btn btn-primary">Masukkan ke Daftar</a>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                
                
            </div>

        </div>

        
    
    
    </div>     
        