<?php  
if (!isset($_SESSION['user_id'])) {
    header('Location: ' . BASEURL);
    exit;
}

?>


<!DOCTYPE html>
<html>
    <head>
       

    <title><?= $data['judul']; ?></title>
        <link rel="stylesheet" href="<?= BASEURL; ?>/fonts/css/all.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?= BASEURL; ?>/css/test.css">
        <link href="<?= BASEURL; ?>/css/sb-admin-2.min.css" rel="stylesheet">
        

    </head>
    <body id="page-top">
        <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                    <i class="fas fa-feather-alt"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Tores Web</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="<?= BASEURL; ?>/galeriUser">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Produk</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item <?= $data['baju_state']; ?>">
                <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-tshirt"></i>
                    <span>Sewa Baju</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Pakaian</h6>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeriUser/sortKebaya">Kebaya</a>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeriUser/sortBajuJas">Baju Jas</a>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeriUser/sortBajuPengantin">Baju Pengantin</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item <?= $data['makeUp_state']; ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMakeUp"
                    aria-expanded="true" aria-controls="collapseMakeUp">
                    <i class="fas fa-eye"></i>
                    <span>Jasa Make Up</span>
                </a>
                <div id="collapseMakeUp" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Make Up</h6>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeriUser/sortTRPengantin">Make Up Pengantin</a>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeriUser/sortTRKhitanan">Make Up Khitanan</a>
                    </div>
                </div>
            </li>
            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item <?= $data['salon_state']; ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-cut"></i>
                    <span>Jasa Salon</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Salon</h6>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeriUser/salon">Salon</a>
                    </div>
                </div>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">           

        </ul>
        <!-- End of Sidebar -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div id="content">
              
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <div class="container">

                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav ml-auto">
                            <a class="nav-link nav" href="<?= BASEURL; ?>/galeriUser">Produk</a>                            
                            <a class="nav-link nav" href="<?= BASEURL; ?>/userHistory">History</a>
                            <a href="<?= BASEURL; ?>/user_cart" class="nav-link nav">Cart : <?= $data['cart']; ?></a>    
                            <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user fa-fw mr-3"></i>
                                <?= $data['user']['nama']; ?>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">                                                
                                <a class="dropdown-item" href="<?= BASEURL; ?>/login/logout">Logout</a>
                            </div>
                            </div>
                        </div>
                        </div>
                  </nav>                
                        <div class="row">
                            <div class="col-lg-6 mt-4 ml-4">
                                <?php Flasher::registrasiFlash(); ?>
                                <?php Flasher::tambahCart(); ?>
                                <?php Flasher::loginFlash(); ?>
                            </div>
                        </div>                