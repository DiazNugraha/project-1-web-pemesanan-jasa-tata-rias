

<?php 
$id_brg = [];
$jumlah_brg = [];
foreach ($data['product'] as $k=>$brg) {
    $id_brg[] = $brg['id_baju'];
    $jumlah_brg[] = $brg['jumlah'];
}
if ($data['cart'] !== 0) {
    $id_baju = implode(", ", $id_brg);
    $jumlah = implode(", ", $jumlah_brg);    
}

?>

<h1 class="ml-5">Cart</h1>

<!-- Content Row -->

<div class="row mt-4">

<!-- Area Chart -->
<div class="col-xl-7 col-lg-7 ml-5">
    <div class="card shadow">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Cart <?= $data['user']['nama'] ?></h6>
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        
        <div class="card-body">
            <div class="row mt-4 ml-md-3 mr-md-3">
            <!-- Flasher -->
            <div class="row">
                <div class="col-lg-12">
                    <?php Flasher::cartFlash(); ?>
                </div>
            </div>
            <!-- End of flasher -->
                <div class="table-responsive">
                    <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>action</th>
                        </tr>
                    </thead>      
                    <tbody>
                        <?php if(!$data['verify']){ ?>
                        <?php $index = 0; ?>
                        <?php foreach($data['product'] as $brg) : ?>
                        <tr>
                            <td><?= $index += 1; ?></td>
                            <td><?= $brg['nama']; ?></td>
                            <td><?= $brg['jumlah']; ?></td>
                            <td>Rp. <?= $brg['hargaUpdate']; ?></td>
                            <td>                                
                                <a href="<?= BASEURL; ?>/user_cart/editData/<?= $brg['id_baju']; ?>/<?= $brg['id']; ?>" class="badge badge-success disabled" aria-disabled="true">Edit</a>
                                <a href="<?= BASEURL; ?>/user_cart/deleteData/<?= $brg['id']; ?>" class="badge badge-danger" onclick="return confirm('Anda yakin?');">Delete</a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <?php } ?>
                        <?php if($data['verify']) {?>
                        <?php $index = 0; ?>
                        <?php foreach($data['product'] as $brg) : ?>
                        <tr>
                            <td><?= $index += 1; ?></td>
                            <td><?= $brg['nama']; ?></td>
                            <td><?= $brg['jumlah']; ?></td>
                            <td>Rp. <?= $brg['hargaUpdate']; ?></td>
                            <td>
                                <!-- <a href="<?= BASEURL; ?>/user_cart/detailData/<?= $brg['id']; ?>" class="badge badge-primary">Detail</a> -->
                                <!-- <a href="<?= BASEURL; ?>/user_cart/editData/<?= $brg['id']; ?>" class="badge badge-success">Edit</a>
                                <a href="<?= BASEURL; ?>/user_cart/deleteData/<?= $brg['id']; ?>" class="badge badge-danger" onclick="return confirm('Anda yakin?');" >Delete</a> -->
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <?php } ?>  
                    </tbody>                                 
                            <tr>
                                <td colspan="3" class="text-right">Total : </td>
                                <td>Rp. <?= $data['total']; ?></td>
                                <td>
                                    <a href="<?= BASEURL; ?>/user_cart/verified" class="btn btn-primary btn-lg btn-block">Verify</a>
                                </td>
                            </tr>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php $totalHarga = (string)$data['total']; ?>

<!-- Pie Chart -->
<!-- Not Edit -->
<?php if ( $data['edit'] ) { ?>
<div class="col-xl-4 ">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"><?= $data['select']['nama']; ?></h6>
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">            
            <!-- <img src="<?= BASEURL; ?>/img/image_viewer.svg" alt="..." class="img-thumbnail" style="width:300px; height:300px;" > -->
                <div class="text-center">
                    <img src="<?= BASEURL; ?>/img/barang/<?= $data['select']['gambar']; ?>" class="" style="height:250px; width:200px;" alt="<?= $data['select']['gambar']; ?>">
                </div>            
                <div class="mt-4 text-center small">
                    <span class="mr-2">
                        <i class="fas fa-circle text-primary"></i>
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-success"></i>
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-info"></i>
                    </span>
                </div>
                <br>
                
                <form action="<?= BASEURL; ?>/user_cart/editDataCart" class="text-center" method="post">
                    <label for="jumlah" class="text-left">Jumlah</label>
                    <input type="hidden" value="<?= $data['select']['harga']; ?>" name="hargaBarang">
                    <input type="hidden" value="<?= $data['select']['id']; ?>" name="barangId">                    
                    <input type="hidden" name="jumlahBaju" id="jumlahBaju" value="<?= $data['baju']['jumlah'] ?>">
                    <input class="form-control" type="text" placeholder="<?= $data['select']['jumlah'] ?>" id="jumlah" name="updateJumlah">
                    <input type="submit" class="btn btn-outline-primary mt-2" value="Submit">
                </form>
        </div>
    </div>
</div>
</div>
<?php } ?>
<!-- End of not edit -->
<!-- Edit -->
<?php if(!$data['edit']){ ?>
    <div class="col-xl-4">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Edit Barang</h6>
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            <div class="chart-pie pt-4 pb-2">
            <!-- <img src="<?= BASEURL; ?>/img/image_viewer.svg" alt="..." class="img-thumbnail" style="width:300px; height:300px;" > -->
                <div class="text-center">
                    <img src="<?= BASEURL; ?>/img/image_viewer.svg" class="" alt="...">
                </div>
            </div>
            <div class="mt-4 text-center small">
                <span class="mr-2">
                    <i class="fas fa-circle text-primary"></i>
                </span>
                <span class="mr-2">
                    <i class="fas fa-circle text-success"></i> 
                </span>
                <span class="mr-2">
                    <i class="fas fa-circle text-info"></i>
                </span>
            </div>
        </div>
    </div>
</div>
</div>
<?php } ?>
<!-- End of edit -->

<!-- Verified -->
<?php if($data['verify']) {?>
<div class="row">
    <div class="col-lg-6 mt-4 ml-4">
        <?php Flasher::checkoutFlash(); ?>
    </div>
</div>
<div class="row mt-4">

<!-- Area Chart -->
<div class="col-xl-7 col-lg-7 ml-5">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Checkout</h6>
            
            <div class="dropdown no-arrow">
            <a type="button" href="<?= BASEURL; ?>/user_cart" class="btn btn-danger">Batal</a>
            </div>
        </div>
        <!-- Card Body -->
        
        <div class="card-body">
            <div class="row mt-4 ml-md-3 mr-md-3">
            <!-- Flasher -->
            <div class="row">
                <div class="col-lg-12">
                    <?php Flasher::cartFlash(); ?>
                </div>
            </div>
            <!-- End of flasher -->
                <!-- Checkout content -->
                <form action="<?= BASEURL; ?>/user_cart/checkout" class="text-center" method="post">                                
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="name">Nama Pemesan </label>
                                <input class="form-control" value="<?= $data['user']['nama']; ?>" name="nama" type="text" id="name" readonly>
                                <label for="">Total Harga</label>
                                <input class="form-control" name="harga" value="<?= $totalHarga; ?>" type="text" id="name" readonly>
                            </div>                    
                            <input type="hidden" name="id_baju" id="barang" value="<?= $id_baju; ?>">
                            <input type="hidden" name="jumlah" id="jumlah" value="<?= $jumlah; ?>">                            
                            <input type="hidden" name="id_user" id="user" value="<?= $data['user']['id_user']; ?>">
                            <input type="hidden" name="no_telp" id="telepon" value="<?= $data['user']['no_telp']; ?>">
                            <input type="hidden" name="email" id="email" value="<?= $data['user']['email']; ?>">
                            <div class="col-md-4">
                                <label for="bookingDate">Tanggal booking</label>
                                <input type="text" class="form-control" name="tanggal_booking" id="bookingDate" value="<?= date('Y-m-d'); ?>" readonly>
                                <label for="time">Lama peminjaman (hari)</label>
                                <input type="number" name="time" id="time" class="form-control" required>
                            </div>
                            <div class="col-md-4">
                                <label for="date">Tanggal Pinjam</label>
                                <input type="date" name="tanggal_pinjam" id="date" class="form-control" required>                        
                            </div>
                            
                        </div>
                            <br>
                            
                                <label for="exampleFormControlTextarea1">Alamat</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat" required></textarea>
                    
                                <br>
                                <button type="submit" value="submit" class="form-control btn btn-primary btn-lg btn-block">Simpan</button>
                    </div>
                </form>                                    
            </div>
        </div>
    </div>
</div>

<br>
<?php } ?>
<?php 


?>
