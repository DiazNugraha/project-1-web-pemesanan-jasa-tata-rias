<div class="container">
<div class="row mt-4">

<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                        <a href="<?= BASEURL; ?>/galeriUser/baju">Sewa Baju</a>                                            
                    </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $data['jumlah_baju']; ?> Jenis Baju</div>
                </div>
                <div class="col-auto">
                    <!-- <i class="fas fa-calendar fa-2x text-gray-300"></i> -->
                    <i class="fas fa-tshirt fa-2x text-gray-300"></i>     
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of earnings monthly -->

<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                        <a href="<?= BASEURL; ?>/galeriUser/tata_rias">Jasa Tata Rias</a>                                            
                    </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $data['jumlah_tata_rias']; ?> Jasa Tata Rias</div>
                </div>
                <div class="col-auto">
                    <!-- <i class="fas fa-calendar fa-2x text-gray-300"></i> -->
                    <i class="fas fa-magic fa-2x text-gray-300"></i>     
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of earnings monthly -->

<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-5 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                        <a href="<?= BASEURL; ?>/galeriUser/salon">Jasa Salon</a>                                            
                    </div>
                    <div class="h6 mb-0 font-weight-bold text-gray-800">Desa drunten kulon, blok tegal pelem, rt/rw 08/02, kec. Gabus wetan, indramayu</div>
                </div>
                <div class="col-auto">
                    <!-- <i class="fas fa-calendar fa-2x text-gray-300"></i> -->
                    <i class="fas fa-cut fa-2x text-gray-300"></i>     
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- end of earnings monthly -->
                <div class="row ml-4">
                
                <!-- Area Chart -->
                <div class="col-xl-11 col-lg-7">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div
                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary"><?= $data['header']; ?></h6>
                            <div class="dropdown no-arrow">
                               
                            </div>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
        
                            <div class="row mt-5 ml-md-3">
                            <!-- All -->
                            <?php if ( $data['type'] == 'all' ) { ?>
                                            
                            <?php foreach($data['baju'] as $baju) : ?>
                            <div class="col-3">
                                <div class="card">
                                    <img src="<?= BASEURL; ?>/img/barang/<?= $baju['gambar']; ?>" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h3><?= $baju['nama']; ?></h3>
                                        <p class="card-text">Jumlah Tersedia : <?= $baju['jumlah']; ?></p>
                                        <p class="card-text">Harga sewa per hari : Rp.<?= number_format($baju['harga'], 2); ?></p>                                        
                                        <a href="<?= BASEURL; ?>/user_cart/tambahBajuCart/<?=  $baju['id']; ?>" class="btn btn-success btn-lg btn-block">Ambil</a>
                                        
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <?php foreach ($data['tata_rias'] as $tata_rias) : ?>                                
                                <div class="col-3">
                                    <div class="card">
                                        <img src="<?= BASEURL; ?>/img/barang/<?= $tata_rias['gambar']; ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h3><?= $tata_rias['nama']; ?></h3>
                                                <p class="card-text">Jenis jasa : <?= $tata_rias['jenis']; ?></p>
                                                <p class="card-text">Status : <?= $tata_rias['status']; ?></p>                                                                                                                                    
                                        </div>
                                    </div>
                                </div>                            
                            <?php endforeach; ?>
                            <?php foreach ($data['salon'] as $salon) : ?>
                                <div class="col-3">
                                    <div class="card">
                                        <img src="<?= BASEURL; ?>/img/barang/<?= $salon['gambar']; ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h3><?= $salon['nama']; ?></h3>
                                                <p class="card-text">Jenis jasa : <?= $salon['jenis']; ?></p>
                                                <p class="card-text">Tempat : <?= $salon['tempat']; ?></p>                                           
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <?php } ?>

                                <!-- End of all -->

                            <?php if( $data['type'] == 'single_baju' ) { ?>
                                <?php foreach ($data['barang'] as $brg) : ?>
                                <div class="col-3">
                                    <div class="card">
                                        <img src="<?= BASEURL; ?>/img/barang/<?= $brg['gambar']; ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h3><?= $brg['nama']; ?></h3>
                                            <p class="card-text">jumlah Tersedia <?= $brg['jumlah']; ?></p>
                                            <p class="card-text">harga sewa per hari : Rp.<?= number_format($brg['harga'], 2); ?></p>                                            
                                            <a href="<?= BASEURL; ?>/user_cart/tambahBajuCart/<?=  $brg['id']; ?>" class="btn btn-success btn-lg btn-block">Ambil</a>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            <?php } ?>
                            <?php if($data['type'] == 'single_makeUp') { ?>
                                <?php foreach ($data['barang'] as $brg) : ?>
                                <div class="col-3">
                                    <div class="card">
                                        <img src="<?= BASEURL; ?>/img/barang/<?= $brg['gambar']; ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h3><?= $brg['nama']; ?></h3>
                                                <p class="card-text">Jenis jasa : <?= $brg['jenis']; ?></p>
                                                <p class="card-text">Status : <?= $brg['status']; ?></p>                                                                                                                                    
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            <?php } ?>
                            <?php if($data['type'] == 'single_salon') { ?>
                                <?php foreach ($data['barang'] as $brg) : ?>
                                <div class="col-3">
                                    <div class="card">
                                        <img src="<?= BASEURL; ?>/img/barang/<?= $brg['gambar']; ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h3><?= $brg['nama']; ?></h3>
                                                <p class="card-text">Jenis jasa : <?= $brg['jenis']; ?></p>
                                                <p class="card-text">Tempat : <?= $brg['tempat']; ?></p>                                           
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            <?php } ?>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
        
        
    </div>

</div>




</div>     
