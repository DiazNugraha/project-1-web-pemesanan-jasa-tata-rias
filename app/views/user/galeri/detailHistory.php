
<h1 class="ml-5">History</h1>

<!-- Content Row -->

<!-- <div class="row mt-4">
<div class="row">
    <div class="col-lg-6 mt-4 ml-4">
        <?php // Flasher::checkoutFlash(); ?>
    </div>
</div> -->
<div class="row mt-4">

<!-- Area Chart -->
<div class="col-xl-11 col-lg-7 ml-5">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Cart <?= $data['user']['nama'] ?></h6>
            <div class="dropdown no-arrow">                
                <a href="<?= BASEURL; ?>/userHistory" class="btn btn-danger text-right">
                <i class="fas fa-arrow-alt-circle-left"></i>
                    Back
                </a>
            </div>
        </div>
        <!-- Card Body -->
        
        <div class="card-body">
            <div class="row mt-4 ml-md-3 mr-md-3">
          
                <div class="table-responsive">
                    <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                   
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jenis</th>
                            <th>Harga Satuan (per hari)</th>
                            <th>Jumlah Pesan</th>
                            <th>Gambar</th>
                        </tr>
                    </thead>      
                    <tbody>
                    <?php 
                    $index = 0;
                    foreach($data['data_baju'] as $usr):                         
                        ?>
                        <tr>
                            <td><?= $index += 1; ?></td>
                            <td><?= $usr['nama']; ?></td>
                            <td><?= $usr['jumlah']; ?></td>
                            <td><?= $usr['harga']; ?></td>
                            <td><?= $data['data_jumlah'][$index - 1]; ?></td>                    
                            <td>
                            <img src="<?= BASEURL; ?>/img/barang/<?= $usr['gambar']; ?>" alt="..." class="rounded mx-auto d-block" style="width:10%;height:100px;">
                            </td>
                            
                        </tr>
                    <?php endforeach; ?>
                     
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Pie Chart -->
<!-- Not Edit -->


<!-- End of not edit -->


