
<?php 


if (isset($_SESSION['logged'])) {
    $_SESSION['user'] = $data['user']['email'];    
}
else{
    header("Location: " . BASEURL);
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            <?= $data['judul']; ?>
        </title>
        <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?= BASEURL; ?>/fonts/css/all.css">
        <link rel="stylesheet" href="<?= BASEURL; ?>/css/<?= $data['styles']; ?>">


    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                <a class="navbar-brand" href="#">Tores Web</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ml-auto">
                    <a class="nav-link active" href="<?= BASEURL; ?>/homeUser">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-link" href="<?= BASEURL; ?>/galeriUser">Produk</a>
                    <!-- <a class="nav-link" href="<?= BASEURL; ?>/form_pemesanan">Form Pesan</a> -->
                    <!-- <a href="#" class="nav-link">Features</a>
                    <a class="nav-link" href="#">Contact Us</a> -->
                    <!-- <a class="nav-item btn tombol" href="<?= BASEURL; ?>/login">Sign In or Sign Up</a> -->
                    
                    <div class="dropdown mr-1">
                        <button type="button" class="btn btn-secondary dropdown-toggle" id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="10,20">
                            <i class="fas fa-user fa-fw mr-3"></i>
                        </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
                        <a class="dropdown-item" href="#">Ganti Password</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </div> 
                </div>
                </div>
                </div>
          </nav>
          