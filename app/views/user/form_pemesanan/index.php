<br>
<br>
<div class="container">
<div class="card ml-5 mr-5 mb-5 bg-light">
<!-- <h3 class="text-left ml-2 mb-3">FORM PEMESANAN</h3> -->
<h3>Form Pemesanan</h3>
        <div class="card-body">
            
            <form>
                <div class="form-group row">
                  <label for="inputNama" class="col-sm-2 col-form-label">Nama Pemesan</label>
                  <div class="col-sm-10">
                    <input type="nama" class="form-control" id="inputNama" placeholder="masukkan nama anda...">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail" placeholder="masukkan email anda...">
                  </div>
                </div>
                <div class="form-group row">
                    <label for="inputAlamat" class="col-sm-2 col-form-label">Alamat Lengkap</label>
                    <div class="col-sm-10">
                      <input type="alamat" class="form-control" id="inputAlamat" placeholder="masukkan alamat anda...">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputKota" class="col-sm-2 col-form-label">Kota</label>
                    <div class="col-sm-10">
                      <input type="Kota" class="form-control" id="inputKota" placeholder="masukkan nama kota...">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputKodepos" class="col-sm-2 col-form-label">Kode Pos</label>
                    <div class="col-sm-10">
                      <input type="kodepos" class="form-control" id="inputKodepos" placeholder="masukkan kode pos...">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputNomerhp" class="col-sm-2 col-form-label">Nomer Hp</label>
                    <div class="col-sm-10">
                      <input type="nomerhp" class="form-control" id="inputNomerhp" placeholder="masukkan nomer hp...">
                    </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-info">Kirim</button>
                  </div>
                </div>
              </form>
        </div>
      </div>
      </div>

 