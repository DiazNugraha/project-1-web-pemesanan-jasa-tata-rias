
<h1 class="ml-5">Dashboard</h1>

<div class="container">
<div class="row mt-4">

<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                        <a href="<?= BASEURL; ?>/galeriUser/baju">Produk</a>                                            
                    </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $data['count_product']; ?> Jenis Baju</div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-calendar fa-2x text-gray-300"></i>                
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of earnings monthly -->


<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                        <a href="<?= BASEURL; ?>/galeriUser/salon">Pendapatan</a>                                            
                    </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">Rp.<?= number_format( $data['pendapatan'], 2)?></div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>                
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                        <a href="<?= BASEURL; ?>/galeriUser/tata_rias">Diproses</a>                                            
                    </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $data['proceeded']; ?> Customer</div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-clipboard fa-2x text-gray-300"></i>                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of earnings monthly -->
<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                        <a href="<?= BASEURL; ?>/galeriUser/tata_rias">Pending</a>                                            
                    </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $data['jumlah']; ?> Customer</div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-clipboard fa-2x text-gray-300"></i>                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of earnings monthly -->
</div>
</div>
<!-- end of earnings monthly -->


<!-- Content Row -->

<div class="row">
    <div class="col-lg-6 mt-4 ml-4">
        <?php  Flasher::checkoutFlash(); ?>
    </div>
</div>
<div class="row mt-4">
<!-- Area Chart -->
<div class="col-xl-7 col-lg-7 ml-5">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"> Grafik</h6>
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        
        <div class="card-body text-center">
            <div class="row mt-4 ml-md-3 mr-md-3">
                <div class="text-center">
                    <img src="<?= BASEURL; ?>/img/dashboard.svg" class="" style="height:250px; width:600px;" alt="">
                </div>                        
            </div>
        </div>
    </div>
</div>
<!-- Pie Chart -->
<!-- Not Edit -->

<div class="col-xl-4">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Admin</h6>
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">            
            <!-- <img src="<?= BASEURL; ?>/img/image_viewer.svg" alt="..." class="img-thumbnail" style="width:300px; height:300px;" > -->
                <div class="text-center">
                    <img src="<?= BASEURL; ?>/img/pie_graph.svg" class="" style="height:250px; width:200px;" alt="">
                </div>            
                <div class="mt-4 text-center small">
                    <span class="mr-2">
                        <i class="fas fa-circle text-primary"></i>
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-success"></i>
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-info"></i>
                    </span>
                </div>
                
        </div>
    </div>
</div>
</div>

<!-- End of not edit -->


