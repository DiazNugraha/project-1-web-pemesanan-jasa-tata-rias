
<h1 class="ml-5">Produk</h1>
<button style="margin-bottom:20px" data-toggle="modal" data-target="#myModal" class="btn btn-info col-md-2 ml-5"><span class="glyphicon glyphicon-plus"></span>Tambah Barang</button>
<!-- Content Row -->

<div class="row">
    <div class="col-lg-6 mt-2 ml-4">
        <?php 
        Flasher::proceedFlash(); 
        Flasher::editProductFlash();
        Flasher::tambahProductFlash();
        Flasher::deleteProductFlash();
        ?>        
    </div>
</div>
<div class="row mt-4">



<!-- Area Chart -->
<div class="col-xl-11 col-lg-7 ml-5">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"> Pakaian</h6>
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        
        <div class="card-body">
            <div class="row mt-4 ml-md-3 mr-md-3">
          
                <div class="table-responsive">
                    <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                   
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jenis</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>      
                    <tbody>
                    <?php
                    
                    $index = 0;                    
                    foreach($data['product'] as $product) :
                    ?>
                        <tr>
                            <td><?= $index += 1; ?></td>
                            <td><?= $product['nama']; ?></td>
                            <td><?= $product['jenis']; ?></td>
                            <td><?= $product['jumlah']; ?></td>
                            <td>Rp. <?= number_format($product['harga'], 2); ?></td>
                            <td>
                                <a href="<?= BASEURL; ?>/adminProduct/edit/<?= $product['id']; ?>" class="btn btn-success">Ubah</a>
                                <a href="<?= BASEURL; ?>/adminProduct/details/<?= $product['id']; ?>" class="btn btn-primary">Detail</a>
                                <a href="<?= BASEURL; ?>/adminProduct/deleteProduct/<?= $product['id']; ?>" class="btn btn-danger">Hapus</a>
                            </td>
                        </tr>
                    <?php 
                    endforeach;                    
                    ?>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Pie Chart -->
<!-- Not Edit -->


<!-- End of not edit -->

<!-- modal box -->
<!-- modal -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Baju</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<form action="<?= BASEURL; ?>/adminProduct/tambahProduct" method="post">
					<div class="form-group">
						<label>Nama Baju</label>
						<input name="nama_barang" type="text" class="form-control" placeholder="Nama Baju ..">
					</div>
					<div class="form-group">
						<label>Jenis Baju</label>
						<input name="jenis_barang" type="text" class="form-control" placeholder="Jenis Baju ..">
					</div>																	
                    <div class="form-group">
						<label>Jumlah Baju</label>
						<input name="jumlah_barang" type="number" class="form-control" placeholder="Jumlah Baju ..">
					</div>	
                    <div class="form-group">
						<label>Harga sewa Baju per hari</label>
						<input name="harga_barang" type="number" class="form-control" placeholder="Harga Sewa ..">
					</div>
                    <div class="form-group">
						<label>Gambar Baju</label>
						<input name="gambar_barang" type="file" class="form-control" placeholder="Gambar ..">
					</div>															

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" value="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>


