<div class="col">
    <div class="row">
        <div class="col-lg-10">
            <h1 class="ml-5">Details</h1>
        </div>
        <div class="col-lg-2">
            <a href="<?= BASEURL; ?>/adminProduct" class="btn btn-danger text-right">
                <i class="fas fa-arrow-alt-circle-left"></i>
                Back
            </a>
        </div>
    </div>
</div>
<!-- Content Row -->

<div class="row">
    <div class="col-lg-6 mt-4 ml-4">
        <?php  Flasher::editProductFlash(); ?>
    </div>
</div>
<div class="row mt-4">
<!-- Area Chart -->
<div class="col-xl-7 col-lg-7 ml-5">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"> Details Baju</h6>
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        
        <div class="card-body">
            <div class="row mt-4 ml-md-3 mr-md-3">
                <form action="<?= BASEURL; ?>/adminProduct/editProduct/<?= $data['product']['id']; ?>" method="post">
                <div class="table-responsive">
                    <table class="table table-bordered text-left" id="dataTable" width="100%" cellspacing="0">                    
                        <tr>
                            <th>Nama Barang</th>
                            <td><?= $data['product']['nama']; ?></td>                            
                        </tr>
                        <tr>
                            <th>Jenis</th>
                            <td><?= $data['product']['jenis']; ?></td>
                        </tr>
                        <tr>
                            <th>Jumlah</th>
                            <td>
                                <input type="number" name="jumlah" value="<?= $data['product']['jumlah']; ?>" id="jumlah" placeholder="<?= $data['product']['jumlah']; ?>">
                            </td>
                        </tr>
                        <tr>
                            <th>Harga sewa</th>
                            <td>Rp. 
                                <input type="text" name="harga" id="harga" value="<?= $data['product']['harga']; ?>" placeholder="<?= $data['product']['harga']; ?>">
                            </td>
                        </tr>                        
                    </table>                                        
                </div>                                             
                <button type="submit" value="submit" class="btn btn-primary">Simpan</button>                                                
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Pie Chart -->
<!-- Not Edit -->

<div class="col-xl-4">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Gambar</h6>
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">            
            <!-- <img src="<?= BASEURL; ?>/img/image_viewer.svg" alt="..." class="img-thumbnail" style="width:300px; height:300px;" > -->
                <div class="text-center">
                    <img src="<?= BASEURL; ?>/img/barang/<?= $data['product']['gambar']; ?>" class="" style="height:250px; width:200px;" alt="">
                </div>            
                <div class="mt-4 text-center small">
                    <span class="mr-2">
                        <i class="fas fa-circle text-primary"></i>
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-success"></i>
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-info"></i>
                    </span>
                </div>
                
        </div>
    </div>
</div>
</div>

<!-- End of not edit -->


