
<h1 class="ml-5">Customer</h1>

<!-- Content Row -->

<div class="row">
    <div class="col-lg-6 mt-4 ml-4">
        <?php 
        Flasher::proceedFlash(); 
        Flasher::deleteCustomerFlash();
        ?>        
    </div>
</div>
<div class="row mt-4">



<!-- Area Chart -->
<div class="col-xl-7 col-lg-7 ml-5">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"> admin</h6>
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        
        <div class="card-body">
            <div class="row mt-4 ml-md-3 mr-md-3">
          
                <div class="table-responsive">
                    <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                   
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tanggal Pinjam</th>
                            <th>Harga</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>      
                    <tbody>
                    <?php
                    
                    $index = 0;                    
                    foreach($data['customer'] as $customer) :
                    ?>
                        <tr>
                            <td><?= $index += 1; ?></td>
                            <td><?= $customer['nama']; ?></td>
                            <td><?= $customer['tanggal_pinjam']; ?></td>
                            <td>Rp.<?= number_format($customer['harga'], 2); ?></td>
                            <td><?php
                             if ($customer['status'] == 0) {
                                 echo 'Menunggu';                                
                             }
                             if ($customer['status'] == 2) {
                                 echo 'terlewat';
                             }
                             if ($customer['status'] == 1) {
                                 echo 'Diproses';
                             }
                             ?></td>
                             <?php if($customer['status'] == 0){ ?>
                                <td>
                                    <a href="<?= BASEURL; ?>/customer/proceed/<?= $customer['id']; ?>" class="btn btn-primary">Proses</a>
                                </td>
                             <?php } ?>
                             <?php if($customer['status'] == 1){ ?>
                                <td>
                                    <a href="<?= BASEURL; ?>/customer/delete/<?= $customer['id']; ?>" class="btn btn-danger">Hapus</a>
                                </td>
                             <?php } ?>
                             <?php if($customer['status'] == 2){ ?>
                                <td>
                                    <a href="<?= BASEURL; ?>/customer/delete/<?= $customer['id']; ?>" class="btn btn-success">Hapus</a>
                                </td>
                             <?php } ?>
                        </tr>
                    <?php 
                    endforeach;                    
                    ?>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Pie Chart -->
<!-- Not Edit -->

<div class="col-xl-4">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div
            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Customer</h6>
            <div class="dropdown no-arrow">
                
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">            
            <!-- <img src="<?= BASEURL; ?>/img/image_viewer.svg" alt="..." class="img-thumbnail" style="width:300px; height:300px;" > -->
                <div class="text-center">
                    <img src="<?= BASEURL; ?>/img/proceed.svg" class="" style="height:250px; width:200px;" alt="">
                </div>            
                <div class="mt-4 text-center small">
                    <span class="mr-2">
                        <i class="fas fa-circle text-primary"></i>
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-success"></i>
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-info"></i>
                    </span>
                </div>
                
        </div>
    </div>
</div>
</div>

<!-- End of not edit -->


