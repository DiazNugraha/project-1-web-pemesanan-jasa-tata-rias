
<!DOCTYPE html>
<html>
    <head>
       

    <title><?= $data['judul']; ?></title>
        <link rel="stylesheet" href="<?= BASEURL; ?>/fonts/css/all.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?= BASEURL; ?>/css/test.css">
        <link href="<?= BASEURL; ?>/css/sb-admin-2.min.css" rel="stylesheet">        
    </head>
    <body id="page-top">
        <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                    <i class="fas fa-feather-alt"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Tores Web</div>
            </a>
            <br>
            <br>
            <br>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="<?= BASEURL; ?>/adminDashboard">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="<?= BASEURL; ?>/adminProduct">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Produk</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="<?= BASEURL; ?>/adminUser">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Pengguna</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Nav Item - Dashboard -->
             <!-- Heading -->
             <div class="sidebar-heading">
                Pesanan
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-tshirt"></i>
                    <span>Customer</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Pakaian</h6>                        
                        <a class="collapse-item" href="<?= BASEURL; ?>/customer">All Customer</a>
                        <a class="collapse-item" href="<?= BASEURL; ?>/customer/pendingRequest">Menunggu</a>
                        <a class="collapse-item" href="<?= BASEURL; ?>/customer/proceededRequest">Diproses</a>
                        <a class="collapse-item" href="<?= BASEURL; ?>/customer/missedRequest">Terlewati</a>
                    </div>
                </div>
            </li>
           
        </ul>
        <!-- End of Sidebar -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">              
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <div class="container">

                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav ml-auto">                        
                            <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user fa-fw mr-3"></i>
                                <?= 'admin ' . $data['admin']['nama']; ?>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">                                                
                                <a class="dropdown-item" href="<?= BASEURL; ?>/login/logout">Logout</a>
                            </div>
                            </div>
                        </div>
                        </div>
                  </nav>                
                        <div class="row">
                            <div class="col-lg-6 mt-4 ml-4">
                                <?php Flasher::registrasiFlash(); ?>
                                <?php Flasher::tambahCart(); ?>                                
                                <?php Flasher::loginAdminFlash(); ?>
                            </div>
                        </div>  
                         