<div class="container">
            <div class="forms-container">
            <!-- Flasher -->
            <div class="row">
                    <div class="col-lg-6">
                        <?php Flasher::loginAdminFlash(); ?>
                    </div>
                </div>
                <!-- /Flasher -->
                <div class="signin-signup">
                
                    <form action="<?= BASEURL; ?>/loginAdmin/login" method="post" class="sign-in-form">
                        <h2 class="title">Sign in</h2>
                        <div class="input-field">
                            <i class="fas fa-envelope"></i>
                            <input type="email" placeholder="Email" name="email">
                        </div>
                        <div class="input-field">
                            <i class="fas fa-lock"></i>
                            <input type="password" placeholder="Password" name="password">
                        </div>
                        <input type="submit" value="login" class="btn solid">
                        <p class="social-text">Or sign in with social platforms</p>
                        <div class="social-media">
                            <a href="" class="social-icon">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="" class="social-icon">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="" class="social-icon">
                                <i class="fab fa-google"></i>
                            </a>
                            <a href="" class="social-icon">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panels-container">
                <div class="panel left-panel">
                    <div class="content">
                        <!-- <h3>New here?</h3>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nulla cumque minima nostrum.</p>
                        <button class="btn transparent" id="sign-up-btn">
                            Sign up
                        </button> -->
                    </div>
                    <img src="<?= BASEURL; ?>/img/login.svg" class="image" alt="">
                </div>
            </div>
        </div>