<!DOCTYPE html>
<html>
    <head>
        <title>
            <?= $data['judul']; ?>
        </title>
        <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?= BASEURL; ?>/fonts/css/all.css">
        <link rel="stylesheet" href="<?= BASEURL; ?>/css/<?= $data['styles']; ?>">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                <a class="navbar-brand" href="#">Tores</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ml-auto">
                    <a class="nav-link active" href="<?= BASEURL; ?>">Home <span class="sr-only">(current)</span></a>
                    <a class="nav-link" href="<?= BASEURL; ?>/galeri">Produk</a>                                        
                    <a class="nav-item btn tombol" href="<?= BASEURL; ?>/login">Join Us</a>
                </div>
                </div>
                </div>
          </nav>