
     <!-- Footer -->
     <footer class="page-footer font-small blue pt-4" style="background-color: #7ed6df;">

<!-- Footer Links -->
<div class="container-fluid text-center text-md-left">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-md-6 mt-md-0 mt-3">

      <!-- Content -->
      <h5 class="text-uppercase">TORES SALON</h5>
      <p>adalah salah satu penyedia jasa tata rias dan penyewaan alat-alatnya </p>

    </div>
    <!-- Grid column -->

    <hr class="clearfix w-100 d-md-none pb-3">

    <!-- Grid column -->
    <div class="col-md-3 mb-md-0 mb-3">

      <!-- Links -->
      <h5 class="text-uppercase">Link to :</h5>

      <ul class="list-unstyled">
        <li>
          <a href="<?= BASEURL; ?>/home">Home</a>
        </li>
        <li>
          <a href="<?= BASEURL; ?>/galeri">Products</a>
        </li>
        <li>
        <br>
          <p>Contact Us : </p>
          <a href="https://wa.me/087732802699" target="_blank">https://wa.me/087732802699</a>
        </li>
      </ul>

    </div>
    <!-- Grid column -->
    <!-- Grid column -->
    <div class="col-md-3 mb-md-0 mb-3">
      <!-- Links -->
      <h5 class="text-uppercase">Links</h5>
      <ul class="list-unstyled">
        <li>
          <a href="<?= BASEURL; ?>/loginAdmin">Login Admin</a>
        </li>       
      </ul>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row -->
</div>
<!-- Footer Links -->
<!-- Copyright -->
<div class="footer-copyright text-center py-3">© 2020 Copyright:
  <a href=""> TORES SALON</a>
</div>
<!-- Copyright -->

</footer>
<!-- Footer -->


<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script> -->

<!-- Option 2: jQuery, Popper.js, and Bootstrap JS
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
-->
<!-- </body>
</html> -->

<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
 -->
<script type="text/javascript" src="<?= BASEURL; ?>/fonts/js/all.js"></script>
 <!-- </div> -->
<script type="text/javascript" src="<?= BASEURL; ?>/js/jquery.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script> -->
<script type="text/javascript" src="<?= BASEURL; ?>/js/bootstrap.bundle.js"></script>
<script type="text/javascript" src="<?= BASEURL; ?>/js/bootstrap.bundle.min.js"></script>
<script src="<?= BASEURL; ?>/js/bootstrap.js"></script>
<script src="<?= BASEURL; ?>/js/script.js"></script>



</body>

</html>