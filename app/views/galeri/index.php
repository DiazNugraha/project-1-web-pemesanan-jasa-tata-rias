
                    <div class="row ml-4">
                
                        <!-- Area Chart -->
                        <div class="col-xl-11 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary"><?= $data['header']; ?></h6>
                                    <div class="dropdown no-arrow">
                                       
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="row mt-5 ml-md-3">
                                    <?php if($data['status'] == 'all') { ?>
                                    <?php foreach($data['barang'] as $brg) : ?>

                                    <div class="col-3">
                                        <div class="card">
                                            <img src="<?= BASEURL; ?>/img/barang/<?= $brg['gambar']; ?>" class="card-img-top" alt="...">
                                            <div class="card-body">
                                            <h3><?= $brg['nama']; ?></h3>
                                                <p class="card-text">Jumlah Tersedia : <?= $brg['jumlah']; ?></p>
                                                <p class="card-text">Harga sewa per hari : Rp.<?= number_format($brg['harga'], 2); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                    <?php foreach($data['makeUp'] as $brg) : ?>

                                    <div class="col-3">
                                        <div class="card">
                                            <img src="<?= BASEURL; ?>/img/barang/<?= $brg['gambar']; ?>" class="card-img-top" alt="...">
                                            <div class="card-body">
                                            <h3><?= $brg['nama']; ?></h3>
                                                <p class="card-text">Jenis jasa : <?= $brg['jenis']; ?></p>
                                                <p class="card-text">Status : <?= $brg['status']; ?></p>                                                                                        
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                    <?php foreach($data['salon'] as $brg) : ?>

                                    <div class="col-3">
                                        <div class="card">
                                            <img src="<?= BASEURL; ?>/img/barang/<?= $brg['gambar']; ?>" class="card-img-top" alt="...">
                                            <div class="card-body">
                                            <h3><?= $brg['nama']; ?></h3>
                                                <p class="card-text">Jenis jasa : <?= $brg['jenis']; ?></p>
                                                <p class="card-text">Tempat : <?= $brg['tempat']; ?></p>                                                                                        
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                    <?php } ?>
                                    <?php if($data['status'] == 'single_baju'){ ?>
                                        <?php foreach($data['barang'] as $brg) : ?>
                                        <div class="col-3">
                                            <div class="card">
                                                <img src="<?= BASEURL; ?>/img/barang/<?= $brg['gambar']; ?>" class="card-img-top" alt="...">
                                                <div class="card-body">
                                                <h3><?= $brg['nama']; ?></h3>
                                                    <p class="card-text">Jumlah Tersedia : <?= $brg['jumlah']; ?></p>
                                                    <p class="card-text">Harga sewa per hari : Rp.<?= number_format($brg['harga']); ?></p>                                                                                        
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                    <?php } ?>
                                    <?php if($data['status'] == 'single_makeUp') { ?>
                                        <?php foreach($data['makeUp'] as $brg) : ?>
                                        <div class="col-3">
                                            <div class="card">
                                                <img src="<?= BASEURL; ?>/img/barang/<?= $brg['gambar']; ?>" class="card-img-top" alt="...">
                                                <div class="card-body">
                                                <h3><?= $brg['nama']; ?></h3>
                                                    <p class="card-text">Jenis jasa : <?= $brg['jenis']; ?></p>
                                                    <p class="card-text">Status : <?= $brg['status']; ?></p>                                                                                        
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                    <?php } ?>
                                    <?php if($data['status'] == 'single_salon') { ?>
                                        <?php foreach($data['salon'] as $brg) : ?>
                                        <div class="col-3">
                                            <div class="card">
                                                <img src="<?= BASEURL; ?>/img/barang/<?= $brg['gambar']; ?>" class="card-img-top" alt="...">
                                                <div class="card-body">
                                                <h3><?= $brg['nama']; ?></h3>
                                                    <p class="card-text">Jenis jasa : <?= $brg['jenis']; ?></p>
                                                    <p class="card-text">Tempat : <?= $brg['tempat']; ?></p>                                                                                        
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                    <?php } ?>
                                    <?php if($data['status'] == 'all_baju') { ?>
                                        <?php foreach($data['barang'] as $brg) : ?>
                                        <div class="col-3">
                                            <div class="card">
                                                <img src="<?= BASEURL; ?>/img/barang/<?= $brg['gambar']; ?>" class="card-img-top" alt="...">
                                                <div class="card-body">
                                                <h3><?= $brg['nama']; ?></h3>
                                                    <p class="card-text">Jumlah Tersedia : <?= $brg['jumlah']; ?></p>
                                                    <p class="card-text">Harga sewa per hari : Rp.<?= number_format($brg['harga']); ?></p>                                                                                        
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                    <?php } ?>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                
                
            </div>

        </div>

        
    
    
    </div>     
     