<!DOCTYPE html>
<html>
    <head>
       

    <title><?= $data['judul']; ?></title>
        <link rel="stylesheet" href="<?= BASEURL; ?>/fonts/css/all.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Viga&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?= BASEURL; ?>/css/test.css">
        <link href="<?= BASEURL; ?>/css/sb-admin-2.min.css" rel="stylesheet">

    </head>
    <body id="page-top">
        <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                    <i class="fas fa-feather-alt"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Tores Web</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="<?= BASEURL; ?>/galeri">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item <?= $data['baju_state']; ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-tshirt"></i>
                    <span>Sewa Baju</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Pakaian</h6>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeri/sortKebaya">Kebaya</a>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeri/sortBajuJas">Baju Jas</a>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeri/sortBajuPengantin">Baju Pengantin</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item <?= $data['makeUp_state']; ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMakeUp"
                    aria-expanded="true" aria-controls="collapseMakeUp">
                    <i class="fas fa-eye"></i>
                    <span>Jasa Make Up</span>
                </a>
                <div id="collapseMakeUp" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Make Up</h6>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeri/sortTRPengantin">Make Up Pengantin</a>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeri/sortTRKhitanan">Make Up Khitanan</a>
                    </div>
                </div>
            </li>
            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item <?= $data['salon_state']; ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-cut"></i>
                    <span>Jasa Salon</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Salon</h6>
                        <a class="collapse-item" href="<?= BASEURL; ?>/galeri/salon">Salon</a>
                    </div>
                </div>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            
            
        </ul>
        <!-- End of Sidebar -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div id="content">
              
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <div class="container">
                      
                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav ml-auto">                            
                            <a class="nav-link nav" href="<?= BASEURL; ?>">Home</a>                            
                            <a class="nav-item btn tombol" href="<?= BASEURL; ?>/login">Join</a>
                        </div>
                        </div>
                        </div>
                  </nav>
                  <div class="container">
                  <div class="row ml-md-3 mt-4">

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            <a href="<?= BASEURL; ?>/galeri/baju">Sewa Baju</a>                                            
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $data['jumlah_baju']; ?> Jenis Baju</div>
                                    </div>
                                    <div class="col-auto">
                                        <!-- <i class="fas fa-calendar fa-2x text-gray-300"></i> -->
                                        <i class="fas fa-tshirt fa-2x text-gray-300"></i>     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of earnings monthly -->
                    
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            <a href="<?= BASEURL; ?>/galeri/tata_rias">Jasa Tata Rias</a>                                            
                                        </div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $data['jumlah_tata_rias']; ?> Jasa Tata Rias</div>
                                    </div>
                                    <div class="col-auto">
                                        <!-- <i class="fas fa-calendar fa-2x text-gray-300"></i> -->
                                        <i class="fas fa-magic fa-2x text-gray-300"></i>     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of earnings monthly -->
                
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-5 col-md-6 mb-4">
                        <div class="card border-left-success shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                            <a href="<?= BASEURL; ?>/galeri/salon">Jasa Salon</a>                                            
                                        </div>
                                        <div class="h6 mb-0 font-weight-bold text-gray-800">Desa drunten kulon, blok tegal pelem, rt/rw 08/02, kec. Gabus wetan, indramayu</div>
                                    </div>
                                    <div class="col-auto">
                                        <!-- <i class="fas fa-calendar fa-2x text-gray-300"></i> -->
                                        <i class="fas fa-cut fa-2x text-gray-300"></i>     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
                    <!-- end of earnings monthly -->