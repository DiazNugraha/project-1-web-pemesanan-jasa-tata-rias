        
        <div class="container">
       
            <div class="forms-container">
            
                <div class="signin-signup">
                
                    <form action="<?= BASEURL; ?>/login/SignIn" class="sign-in-form" method="post">
                     <!-- Flasher -->
                <div class="row">
                    <div class="col-lg-6">
                        <?php Flasher::loginFlash(); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="row-lg-6">
                        <?php Flasher::registrasiFlash(); ?>
                    </div>
                </div>
                <!-- /Flasher -->
                        <h2 class="title">Sign in</h2>
                        <div class="input-field">
                            <i class="fas fa-envelope"></i>
                            <input type="email" placeholder="Email" name="email">
                        </div>
                        <div class="input-field">
                            <i class="fas fa-lock"></i>
                            <input type="password" placeholder="Password" name="password">
                        </div>
                        <input type="submit" value="login" class="btn solid">
                        <p class="social-text">Or Sign in with social platforms</p>
                        <div class="social-media">
                            <a href="#" class="social-icon">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="#" class="social-icon">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="#" class="social-icon">
                                <i class="fab fa-google"></i>
                            </a>
                            <a href="#" class="social-icon">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </div>
                    </form>
                    <form action="<?= BASEURL; ?>/registrasi/TambahUser" method="post" class="sign-up-form">
                        <h2 class="title">Sign up</h2>
                        <div class="input-field">
                            <i class="fas fa-user"></i>
                            <input type="text" placeholder="Username" name="username">
                        </div>
                        <div class="input-field">
                            <i class="fas fa-envelope"></i>
                            <input type="email" placeholder="Email" name="email">
                        </div>
                        <div class="input-field">
                            <i class="fas fa-phone"></i>
                            <input type="text" placeholder="No. Telepon" name="notelp">
                        </div>
                        <div class="input-field">
                            <i class="fas fa-lock"></i>
                            <input type="password" placeholder="Password" name="password">
                        </div>
                        <div class="input-field">
                            <i class="fas fa-lock"></i>
                            <input type="password" placeholder="Ketik ulang password" name="repassword">
                        </div>
                        <input type="submit" value="Sign up" class="btn solid">
                        <p class="social-text">Or Sign up with social platforms</p>
                        <div class="social-media">
                            <a href="#" class="social-icon">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="#" class="social-icon">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="#" class="social-icon">
                                <i class="fab fa-google"></i>
                            </a>
                            <a href="#" class="social-icon">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panels-container">
                <div class="panel left-panel">
                    <div class="content">
                        <h3>Belum punya akun?</h3>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Libero neque tenetur rem?</p>                        
                        <button class="btn transparent" id="sign-up-btn">
                            Sign up
                        </button>
                    </div>
                    <img src="img/login.svg" alt="" class="image">
                </div>
                <div class="panel right-panel">
                    <div class="content">
                        <h3>Sudah punya akun?</h3>
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Libero neque tenetur rem?</p>
                        <button class="btn transparent" id="sign-in-btn">
                            Sign in
                        </button>
                    </div>
                    <img src="img/register.svg" alt="" class="image">
                </div>
            </div>
        </div>