<?php


class Tata_rias_model{
    private $table = 'tata_rias';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllTataRias(){
        $query = 'SELECT * FROM ' . $this->table;
        $this->db->query($query);
        return $this->db->resultSet();
    }

    public function getAllTRPengantin(){
        $jenis_barang = 'Tata Rias Pengantin';
        $query = 'SELECT * FROM ' . $this->table . ' WHERE jenis = :jenis_barang';
        $this->db->query($query);
        $this->db->bind('jenis_barang', $jenis_barang);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getAllTRKhitanan(){
        $jenis_barang = 'Tata Rias Khitanan';
        $query = 'SELECT * FROM ' . $this->table . ' WHERE jenis = :jenis_barang';
        $this->db->query($query);
        $this->db->bind('jenis_barang', $jenis_barang);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getTRById($id){
        $query = 'SELECT * FROM ' . $this->table . 'WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->single();
    }

}