<?php 

class Admin_model {
    private $table = 'admin';
    private $db;

    public function __construct(){
        $this->db = new Database;    
    }

    public function getAdminData($data){        
        $query = 'SELECT * FROM ' . $this->table . ' WHERE email = :email AND password = :password';
        $this->db->query($query);
        $this->db->bind('email', $data['email']);
        $this->db->bind('password', $data['password']);
        $this->db->execute();
        return $this->db->single();
    }

    public function loginAdmin($data){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE email = :email AND password = :password ';        
        $this->db->query($query);
        $this->db->bind('email', $data['email']);
        $this->db->bind('password', $data['password']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function getAdminDataById($id){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->single();
    }

    public function getAllCustomer(){
        $query = 'SELECT * FROM check_out';
        $this->db->query($query);        
        return $this->db->resultSet();
    }

    public function proceedRequest($id){        
        $query = 'UPDATE check_out SET status = :status_update WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->bind('status_update', 1);
        $this->db->execute();
        return $this->db->rowCount();
    }    

    public function getAllPendingCustomer(){
        $query = 'SELECT * FROM check_out WHERE status = :status';
        $this->db->query($query);
        $this->db->bind('status', "0");
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getAllProceededCustomer(){
        $query = 'SELECT * FROM check_out WHERE status = :status';
        $this->db->query($query);
        $this->db->bind('status', "1");
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getPenghasilan(){
        $query = 'SELECT * FROM toko';        
        $this->db->query($query);                
        return $this->db->single();
    }    

    public function tambahPendapatan($data){
        $query = 'UPDATE toko SET pendapatan = :pendapatan WHERE id = 1';
        $this->db->query($query);
        $this->db->bind('pendapatan', $data);
        $this->db->execute();        
    }

}