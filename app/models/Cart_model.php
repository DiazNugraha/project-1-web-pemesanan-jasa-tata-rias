<?php 


class Cart_model{
    private $table = 'user_cart';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function tambahCart($data = []){
        $query = "INSERT INTO user_cart VALUES(0 , :id_user, :nama_barang, :jumlah_barang, :harga_barang, :harga_update,  :gambar, :id_baju, :jumlah_baju)";
        $this->db->query($query);
        $this->db->bind('id_user', $_SESSION['user_id']);
        $this->db->bind('nama_barang', $data['id_baju']['nama']);
        $this->db->bind('jumlah_barang', 1);
        $this->db->bind('harga_barang', $data['id_baju']['harga']);
        $this->db->bind('harga_update', $data['id_baju']['harga']);
        $this->db->bind('gambar', $data['id_baju']['gambar']);
        $this->db->bind('id_baju', $data['id_baju']['id']);
        $this->db->bind('jumlah_baju', $data['id_baju']['jumlah']);
        $this->db->execute();
        return $this->db->rowCount();
        
    }

    public function getAllCart(){
        $query = 'SELECT * FROM user_cart WHERE id_user = :id_user';
        $this->db->query($query);
        $this->db->bind('id_user', $_SESSION['user_id']);
        $this->db->execute();
        return $this->db->resultSet();
        
    }

    public function deleteDataCart($id){
        $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function getCartById($id){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->single();
    }

    public function editDataCartById($data = []){
        $query = 'UPDATE ' . $this->table . ' SET jumlah = :updateJumlah, hargaUpdate = :updateHarga WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('updateJumlah', $data['updateJumlah']);
        $this->db->bind('updateHarga', $data['updateHarga']);
        $this->db->bind('id', $data['id']);
        $this->db->execute();
        return $this->db->rowCount();
        // var_dump($data['updateJumlah']);
        // var_dump($data['updateHarga']);
    }

    public function addToCheckout($data, $harga){
        $query = 'INSERT INTO check_out VALUES(0, :nama, :alamat, :harga, :tanggal_booking, :tanggal_pinjam, :id_baju, :jumlah, :id_user, :no_telp, :email, :status)';
        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('alamat', $data['alamat']);
        $this->db->bind('harga', $harga);
        $this->db->bind('tanggal_booking', $data['tanggal_booking']);
        $this->db->bind('tanggal_pinjam', $data['tanggal_pinjam']);
        $this->db->bind('id_baju', $data['id_baju']);
        $this->db->bind('jumlah', $data['jumlah']);
        $this->db->bind('id_user', $data['id_user']);
        $this->db->bind('no_telp', $data['no_telp']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('status', 0);        
        $this->db->execute();
        return $this->db->rowCount();
    }

}