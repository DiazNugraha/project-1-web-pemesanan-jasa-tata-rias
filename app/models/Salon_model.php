<?php 


class Salon_model{

    private $table = 'salon';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllSalon(){
        $query = 'SELECT * FROM ' . $this->table;
        $this->db->query($query);
        return $this->db->resultSet();
    }
}