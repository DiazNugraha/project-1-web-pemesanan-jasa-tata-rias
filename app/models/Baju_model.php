<?php

class Baju_model{
    private $table = 'baju';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllBaju(){
        $query = 'SELECT * FROM ' . $this->table;
        $this->db->query($query);
        return $this->db->resultSet();
    }

    public function getAllBajuJas(){
        $jenis_barang = 'Baju Jas';
        $query = 'SELECT * FROM ' . $this->table . ' WHERE jenis = :jenis_barang';
        $this->db->query($query);
        $this->db->bind('jenis_barang', $jenis_barang);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getAllKebaya(){
        $jenis_barang = 'Baju Kebaya';
        $query = 'SELECT * FROM ' . $this->table . ' WHERE jenis = :jenis_barang';
        $this->db->query($query);
        $this->db->bind('jenis_barang', $jenis_barang);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getAllBajuPengantin(){
        $jenis_barang = 'Baju Pengantin';
        $query = 'SELECT * FROM ' . $this->table . ' WHERE jenis = :jenis_barang';
        $this->db->query($query);
        $this->db->bind('jenis_barang', $jenis_barang);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getBajuById($data){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $data);
        $this->db->execute();
        return $this->db->single();
    }

    public function getAllBajuById($data){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $data);
        $this->db->execute();
        return $this->db->resultSet();
    }    

    public function getAllGambarById($data){
        $query = 'SELECT gambar FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $data);
        $this->db->execute();
        return $this->db->single();
    }

    public function updateDataBaju($jumlah, $id){
        $query = 'UPDATE ' . $this->table . ' SET jumlah = :jumlah WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('jumlah', $jumlah);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }
    
    public function editDataBaju($id, $data){
        $query = 'UPDATE ' . $this->table . ' SET jumlah = :jumlah, harga = :harga WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('jumlah', $data['jumlah']);
        $this->db->bind('harga', $data['harga']);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();                
    }

    public function getJumlahBajuById($id){
        $query = 'SELECT jumlah FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->single();
    }

    public function deleteCustomer($id_baju, $nilai){
        $query = 'UPDATE ' . $this->table . ' SET jumlah = :jumlah WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('jumlah', $nilai);
        $this->db->bind('id', $id_baju);
        $this->db->execute();        
    }

    public function tambahBaju($data){
        $query = 'INSERT INTO ' . $this->table . ' VALUES(0, :nama_barang, :jenis_barang, :jumlah_barang, :harga_barang, :gambar_barang)';
        $this->db->query($query);
        $this->db->bind('nama_barang', $data['nama_barang']);
        $this->db->bind('jenis_barang', $data['jenis_barang']);
        $this->db->bind('jumlah_barang', $data['jumlah_barang']);
        $this->db->bind('harga_barang', $data['harga_barang']);
        $this->db->bind('gambar_barang', $data['gambar_barang']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function deleteBaju($id){
        $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

}