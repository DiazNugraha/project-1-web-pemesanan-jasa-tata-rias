<?php

class User_model{
    private $table = 'user';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function tambahDataUser($data){
        if ($data['password'] !== $data['repassword']) {
            return 0;
        }

        $query = 'SELECT * FROM ' . $this->table;
        $this->db->query($query);
        $this->db->execute();
        $result = $this->db->resultSet();
        
        foreach($result as $hasil) : 
            if ($data['email'] == $hasil['email']) {
                return 0;
            }
        endforeach;

        $query = "INSERT INTO user
                    VALUES
                  (0, :username, :email, :notelp, :password)";
        
        $passwordhash = md5($data['password']);
        $this->db->query($query);
        $this->db->bind('username', $data['username']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('notelp', $data['notelp']);
        $this->db->bind('password', $passwordhash);
        
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function userLogin($data){        
        $passwordhash = md5($data['password']);        
        $query = "SELECT * FROM user WHERE email = :email AND password = :password";
        $this->db->query($query);
        $this->db->bind('email', $data['email']);
        $this->db->bind('password', $passwordhash);

        $this->db->execute();
        return $this->db->rowCount();
    }
    
    public function getUserData($data){
        $query = "SELECT * FROM user WHERE email = :email";
        $this->db->query($query);
        $this->db->bind('email', $data['email']);
        $this->db->execute();
        return $this->db->single();
    }

    public function getDataUser($data){
        $query = "SELECT * FROM user WHERE email = :email";
        $this->db->query($query);
        $this->db->bind('email', $data);
        $this->db->execute();
        return $this->db->single();
    }

    public function getUserById($id){
        $query = 'SELECT * FROM user WHERE id_user = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->single();
    }

    public function getAllUser(){
        $query = 'SELECT * FROM ' . $this->table;
        $this->db->query($query);
        return $this->db->resultSet();
    }

    public function deleteUser($id){        
        $query = 'DELETE FROM ' . $this->table . ' WHERE id_user = :id_user';
        $this->db->query($query);
        $this->db->bind('id_user', $id);
        $this->db->execute();
        return $this->db->rowCount();        
    }
  
}