<?php 

class Checkout_model{
    private $table = 'check_out';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllCheckout(){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE id_user = :id_user';
        $this->db->query($query);
        $this->db->bind('id_user', $_SESSION['user_id']);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getCheckoutById($id){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->single();        
    }

    public function getAllCustomer(){
        $query = 'SELECT * FROM ' . $this->table;
        $this->db->query($query);
        return $this->db->resultSet();
    }

    public function deleteCustomer($id){
        $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function updateCustomerById($id){
        $query = 'UPDATE ' . $this->table . ' SET status = :status WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('status', 2);
        $this->db->bind('id', $id);
        $this->db->execute();        
    }

}