<?php 

class galeriUser extends Controller{
    public function index(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['status_galeri'] = 'active';
        $data['baju'] = $this->model('Baju_model')->getAllBaju();
        $data['tata_rias'] = $this->model('Tata_rias_model')->getAllTataRias();
        $data['salon'] = $this->model('Salon_model')->getAllSalon();
        $cart = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($cart);
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $data['type'] = 'all';
        $data['header'] = 'Semua Produk';
        $data['styles'] = 'galeri.css';
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/index', $data);
        $this->view('user/galeri/footer');     
    }

    // Tata Rias

    public function tata_rias(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['makeUp_state'] = 'active';
        $data['barang'] = $this->model('Tata_rias_model')->getAllTataRias();
        $data['header'] = 'All Tata Rias';
        $data['type'] = 'single_makeUp';    
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);    
        $cart = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($cart);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/index', $data);
        $this->view('user/galeri/footer');
    }

    public function sortTRPengantin(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['makeUp_state'] = 'active';
        $data['barang'] = $this->model('Tata_rias_model')->getAllTRPengantin();
        $data['header'] = 'All Tata Rias Pengantin';
        $data['type'] = 'single_makeUp';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        // $data['user'] = $this->model('User_model')->getDataUser($_SESSION['user']);
        $cart = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($cart);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/index', $data);
        $this->view('user/galeri/footer');
    }

    public function sortTRKhitanan(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['makeUp_state'] = 'active';
        $data['barang'] = $this->model('Tata_rias_model')->getAllTRKhitanan();
        $data['header'] = 'All Tata Rias Khitanan';
        $data['type'] = 'single_makeUp';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        // $data['user'] = $this->model('User_model')->getDataUser($_SESSION['user']);
        $cart = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($cart);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/index', $data);
        $this->view('user/galeri/footer');   
    }

    // End of tata rias

    // Salon

    public function salon(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['salon_state'] = 'active';
        $data['barang'] = $this->model('Salon_model')->getAllSalon();
        $data['header'] = 'All Salon';
        $data['type'] = 'single_salon';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        // $data['user'] = $this->model('User_model')->getDataUser($_SESSION['user']);
        $cart = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($cart);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/index', $data);
        $this->view('user/galeri/footer');
    }

    // End of salon

    // Baju

    public function baju(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['baju_state'] = 'active';
        // $data['user'] = $this->model('User_model')->getDataUser($_SESSION['user']);
        $data['barang'] = $this->model('Baju_model')->getAllBaju();
        $data['header'] = 'Pakaian';
        $data['type'] = 'single_baju';
        $data['styles'] = 'galeri.css';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $cart = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($cart);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/index', $data);
        $this->view('user/galeri/footer');
    }

    public function detailBaju($id){
        // var_dump($id);
        $data['judul'] = 'TORES WEB || Details Gambar';
        $data['baju_state'] = 'active';
        $data['type'] = 'single';
        $data['barang'] = $this->model('Baju_model')->getBajuById($id);
        // $data['user'] = $this->model('User_model')->getDataUser($_SESSION['user']);
        $cart = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($cart);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/details', $data);
        $this->view('user/galeri/footer');
    }

    public function sortKebaya(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['baju_state'] = 'active';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $data['barang'] = $this->model('Baju_model')->getAllKebaya();
        $data['header'] = 'Semua Kebaya';
        $data['type'] = 'single_baju';        
        $cart = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($cart);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/index', $data);
        $this->view('user/galeri/footer');
    }

    public function sortBajuJas(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['baju_state'] = 'active';
        $data['barang'] = $this->model('Baju_model')->getAllBajuJas();
        $data['header'] = 'Semua Baju Jas';
        $data['type'] = 'single_baju';  
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);      
        $cart = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($cart);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/index', $data);
        $this->view('user/galeri/footer');
    }

    public function sortBajuPengantin(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['baju_state'] = 'active';
        $data['barang'] = $this->model('Baju_model')->getAllBajuPengantin();
        $data['header'] = 'Semua Baju Pengantin';
        $data['type'] = 'single_baju';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        // $data['user'] = $this->model('User_model')->getDataUser($_SESSION['user']);
        $cart = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($cart);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/index', $data);
        $this->view('user/galeri/footer');
    }

    // End of baju

    // Packet
    public function packetPernikahan(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['baju_state'] = 'active';
        $data['type'] = 'packet';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $data['baju_jas'] = $this->model('Baju_model')->getAllBajuJas();
        $data['baju_pengantin'] = $this->model('Baju_model')->getAllBajuPengantin();
        $data['tata_rias'] = $this->model('Tata_rias_model')->getAllTRPengantin();
        $data['header'] = 'All Packet Pengantin';
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/index', $data);
        $this->view('user/galeri/footer');
    }
}


?>