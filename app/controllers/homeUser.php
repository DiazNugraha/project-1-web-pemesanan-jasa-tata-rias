<?php


class homeUser extends Controller{
    public function index(){
        $data['judul'] = 'TORES WEB || Home';
        $data['status_home'] = 'active';
        $data['styles'] = 'landingPage.css';    
        $data['user'] = $this->model('User_model')->getDataUser($_SESSION['user']);
        $this->view('user/templates/header', $data);
        $this->view('user/landingPage/index', $data);
        $this->view('user/templates/footer');
    }
}