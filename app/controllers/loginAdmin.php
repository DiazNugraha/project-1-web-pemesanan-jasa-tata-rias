<?php 

class loginAdmin extends Controller{
    public function index(){
        $data['judul'] = 'TORES WEB || Login Admin';
        $this->view('loginAdmin/header', $data);
        $this->view('loginAdmin/index', $data);
        $this->view('loginAdmin/footer');
    }

    public function login(){  
        session_start();
        $email = $_POST['email'];
        $password = md5($_POST['password']);
        $data['email'] = $email;               
        $data['password'] = $password;        
        if( $hasil = $this->model('Admin_model')->loginAdmin($data) > 0 ){
            Flasher::setloginAdminFlash('Berhasil', 'Login', 'green');
            $data['admin'] = $this->model('Admin_model')->getAdminData($data);
            $_SESSION['admin_id'] = $data['admin']['id'];
            header('Location: ' . BASEURL . '/adminDashboard');
            exit;
        }
        else {
            Flasher::setLoginAdminFlash('Gagal', 'Login', 'red');
            header('Location: ' . BASEURL . '/loginAdmin');
            exit;
        }
            
    }

    public function logout(){
        session_start();
        session_destroy();
        header('location: ' . BASEURL );
    }
}





?>