<?php

class home extends Controller {
    
    public function index(){
        $data['judul'] = 'TORES WEB || Home';
        $data['status_home'] = 'active';
        $data['styles'] = 'landingPage.css';
        $this->view('templates/header', $data);
        $this->view('landingPage/index', $data);
        $this->view('templates/footer');
    }
}