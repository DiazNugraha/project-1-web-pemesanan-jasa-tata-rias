<?php 

class adminUser extends Controller{
    public function index(){
        $data['judul'] = 'Users || Admin';
        $data['user'] = $this->model('User_model')->getAllUser();
        $data['admin'] = $this->model('Admin_model')->getAdminDataById($_SESSION['admin_id']);        
        $this->view('admin/header', $data);
        $this->view('admin/user/index', $data);
        $this->view('admin/footer');
    }

    public function delete($id){
        $data['user'] = $this->model('User_model')->getUserById($id);
        $data['customer'] = $this->model('Checkout_model')->getAllCustomer();
        $hasil = 0;        
        foreach($data['customer'] as $customer){
            if ( $customer['email'] == $data['user']['email'] ) {
                $hasil = 1;
            }            
        }
        if ( $hasil == 1 ) {
            Flasher::setDeleteUserFlash('Gagal', 'Hapus karena user masih berada di checkout', 'danger');
            header('Location: ' . BASEURL . '/adminUser');
            exit;
        }
        if ( $this->model('User_model')->deleteUser($id) > 0 ) {   
            Flasher::setDeleteUserFlash('Berhasil', 'Hapus', 'success');         
            header('Location: ' . BASEURL . '/adminUser');
            exit;
        }
        else {
            Flasher::setDeleteUserFlash('Gagal', 'Hapus', 'danger');
            header('Location: ' . BASEURL . '/adminUser');
            exit;
        }        
    }
}