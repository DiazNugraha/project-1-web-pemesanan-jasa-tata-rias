<?php

class customer extends Controller{
    public function index(){
        $data['judul'] = 'Customer || Admin';        
        $data['customer'] = $this->model('Admin_model')->getAllCustomer();                
        $data['admin'] = $this->model('Admin_model')->getAdminDataById($_SESSION['admin_id']);
        $this->view('admin/header', $data);
        $this->view('admin/customer/customer', $data);
        $this->view('admin/footer');
    }

    public function pendingRequest(){
        $data['judul'] = 'Customer || Admin';        
        $data['customer'] = $this->model('Admin_model')->getAllPendingCustomer();                        
        $data['admin'] = $this->model('Admin_model')->getAdminDataById($_SESSION['admin_id']);
        $this->view('admin/header', $data);
        $this->view('admin/customer/customer', $data);
        $this->view('admin/footer');
    }

    public function proceededRequest(){
        $data['judul'] = 'Customer || Admin';        
        $data['customer'] = $this->model('Admin_model')->getAllProceededCustomer();                        
        $data['admin'] = $this->model('Admin_model')->getAdminDataById($_SESSION['admin_id']);
        $this->view('admin/header', $data);
        $this->view('admin/customer/customer', $data);
        $this->view('admin/footer');
    }

    public function missedRequest(){
        $data['judul'] = 'Customer || Admin';   
        $data['admin'] = $this->model('Admin_model')->getAdminDataById($_SESSION['admin_id']);     
        $data['missed'] = $this->model('Checkout_model')->getAllCustomer();   
        $tgl = date("Y-m-d");
        $tgl = strtotime($tgl);
        $hasil = [];
        foreach($data['missed'] as $cstmr){                        
            $date = strtotime($cstmr['tanggal_pinjam']);
            if ($date < $tgl ) {
                $hasil[] = $cstmr['id'];
            }
        }
        
        foreach($hasil as $hsl){            
            $data['customer'][] = $this->model('Checkout_model')->getCheckoutById($hsl);
            $this->model('Checkout_model')->updateCustomerById($hsl);
        }        
        
        if (!isset($data['customer'])) {
            $data['customer'] = [];
        }
        $this->view('admin/header', $data);
        $this->view('admin/customer/customer', $data);
        $this->view('admin/footer');
        
    }


    public function proceed($id){
        $data['checkout'] = $this->model('Checkout_model')->getCheckoutById($id);
        $data['penghasilan'] = $this->model('Admin_model')->getPenghasilan();
        $pendapatan = $data['penghasilan']['pendapatan'] + $data['checkout']['harga'];
        $data['data_jumlah'] = explode(", ", $data['checkout']['jumlah']);        
        $data['id_baju'] = explode(", ", $data['checkout']['id_baju']);
        $data['hasil']['asd'] = $data['data_jumlah'];                   

        for ($i=0; $i < count($data['hasil']['asd']); $i++) { 
            $data_baju[] = $this->model('Baju_model')->getJumlahBajuById($data['id_baju'][$i]);
        }        
        

        for ($i=0; $i < count($data['hasil']['asd']) ; $i++) { 
            $hasil[] =  $data_baju[$i]['jumlah'] - $data['hasil']['asd'][$i];            
        }                                

        for ($i=0; $i < count($data['hasil']['asd']); $i++) {             
            $this->model('Baju_model')->updateDataBaju($hasil[$i], $data['id_baju'][$i]);
        }                                       
        
        if ( $this->model('Admin_model')->proceedRequest($id) > 0) {
            $this->model('Admin_model')->tambahPendapatan($pendapatan);
            Flasher::setProceedFlash('Berhasil', 'Proses', 'success');
            header('Location: ' . BASEURL . '/customer');
            exit;            
        }else {
            Flasher::setProceedFlash('Gagal', 'Proses', 'danger');
            header('Location: ' . BASEURL . '/customer');
            exit;            
        }
    }

    public function delete($id){
        $data['checkout'] = $this->model('Checkout_model')->getCheckoutById($id);
        $data['data_jumlah'] = explode(", ", $data['checkout']['jumlah']);        
        $data['id_baju'] = explode(", ", $data['checkout']['id_baju']);

        $data['tambah'] = [
            'id_baju' => $data['id_baju'],
            'data_jumlah' => $data['data_jumlah']            
        ];     

        for ($i=0; $i < count($data['tambah']['id_baju']); $i++) { 
            $jumlah_baju[] = $this->model('Baju_model')->getJumlahBajuById($data['tambah']['id_baju'][$i]);        
        }        

        for ($i=0; $i < count($data['tambah']['id_baju']); $i++) { 
            $result[] = $data['tambah']['data_jumlah'][$i] + $jumlah_baju[$i]['jumlah'];
        }

        for ($i=0; $i < count($data['tambah']['id_baju']); $i++) { 
            $this->model('Baju_model')->deleteCustomer($data['tambah']['id_baju'][$i], $result[$i]);
        }

        if ( $this->model('Checkout_model')->deleteCustomer($id) > 0 ) {
            Flasher::setDeleteCustomerFlash('Berhasil', 'hapus', 'success');
            header('Location: ' . BASEURL . '/customer');
            exit;            
        }
        else {
            Flasher::setDeleteCustomerFlash('Gagal', 'hapus', 'danger');
            header('Location: ' . BASEURL . '/customer');
            exit;
        }
                
    }

    
}