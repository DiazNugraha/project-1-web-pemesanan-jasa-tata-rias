<?php


class login extends Controller{
    public function index(){
        $data['judul'] = 'TORES WEB || Login';
        $this->view('login/header', $data);
        $this->view('login/index', $data);
        $this->view('login/footer');
    }

    public function SignIn(){
        session_start();
        if ($this->model('User_model')->userLogin($_POST) > 0) {
            Flasher::setLoginFlash('Berhasil', 'Login', 'green');
            $data['user'] = $this->model('User_model')->getUserData($_POST);
            $_SESSION['user_id'] = $data['user']['id_user'];
            header('Location:' . BASEURL . '/galeriUser');
            exit;
        }
        else {

            Flasher::setLoginFlash('Gagal', 'Login', 'red');
            header('Location:' . BASEURL . '/login');
            exit;
        }    
    }

    public function logout(){
        session_start();
        session_destroy();
        header('Location: ' . BASEURL);
    }


}