<?php 


class user_cart extends Controller{
    public function index(){
        $data['judul'] = 'TORES WEB || Cart';
        $data['status_galeri'] = 'active';
        $data['product'] = $this->model('Cart_model')->getAllCart();
        $data['select'] = '';
        $data['verify'] = false;
        $data['cart'] = count($data['product']);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $data['edit'] = false;
        $parse = (integer)$data['product'];
        $total = 0;
        foreach ($data['product'] as $product) {
            $int = (integer)$product['hargaUpdate'];
            $total += $int;
        }
        $data['total'] = $total;
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/cartUser', $data);
        $this->view('user/galeri/footer');
    }

    public function tambahBajuCart($id){
        $data['id_baju'] = $this->model('Baju_model')->getBajuById($id);                
        if ( $this->model('Cart_model')->tambahCart($data) > 0 ) {
            Flasher::setTambahCart('Berhasil', 'Tambah', 'success');
            header('Location:' . BASEURL . '/galeriUser');
            exit;
        }
        else{
            Flasher::setTambahCart('Gagal', 'Tambah', 'danger');
            header('Location: ' . BASEURL . '/galeriUser');
        }
    }

    public function deleteData($id){        
        if ( $this->model('Cart_model')->deleteDataCart($id) > 0 ) {
            Flasher::setCartFlash('Berhasil', 'Hapus', 'success');
            header('Location: ' . BASEURL . '/user_cart');
            exit;
        }
        else{
            Flasher::setCartFlash('Gagal', 'Hapus', 'danger');
            header('Location: ' . BASEURL . '/user_cart');
            exit;
        }
    }

    public function editData($id_baju, $id){        
        $data['judul'] = 'TORES WEB || Cart';
        $data['status_galeri'] = 'active';
        $data['product'] = $this->model('Cart_model')->getAllCart();
        $data['select'] = $this->model('Cart_model')->getCartById($id);
        $data['baju'] = $this->model('Baju_model')->getBajuById($id_baju);        
        $data['cart'] = count($data['product']);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $data['edit'] = true;
        $data['verify'] = false;
        $parse = (integer)$data['product'];
        $total = 0;
        foreach ($data['product'] as $product) {
            $int = (integer)$product['hargaUpdate'];
            $total += $int;
        }
        $data['total'] = $total;
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/cartUser', $data);
        $this->view('user/galeri/footer');   
    }

    public function editDataCart(){
        $data['id'] = $_POST['barangId'];
        $data['updateJumlah'] = (integer)$_POST['updateJumlah'];
        $data['harga'] = (integer)$_POST['hargaBarang'];                    
        $hasil = $_POST['jumlahBaju'] - $data['updateJumlah'];        
        if ( $hasil <= 1) {
            Flasher::setCartFlash('Gagal', 'Update', 'danger');
            header('Location: ' . BASEURL . '/user_cart');
            exit;
        }                
        $data['updateHarga'] = $data['updateJumlah'] * $data['harga'];
        if ( $this->model('Cart_model')->editDataCartById($data) ) {
            Flasher::setCartFlash('Berhasil', 'Update', 'success');
            header('Location: ' . BASEURL . '/user_cart');
            exit;
        }
        else{
            Flasher::setCartFlash('Gagal', 'Update', 'danger');
            header('Location: ' . BASEURL . '/user_cart');
            exit;
        }
    }

    public function verified(){
        $data['judul'] = 'TORES WEB || Cart';
        $data['status_galeri'] = 'active';
        $data['product'] = $this->model('Cart_model')->getAllCart();
        $data['select'] = '';
        $data['cart'] = count($data['product']);
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $data['edit'] = false;
        $data['verify'] = true;
        $parse = (integer)$data['product'];
        $total = 0;
        foreach ($data['product'] as $product) {
            $int = (integer)$product['hargaUpdate'];
            $total += $int;
        }
        $data['total'] = $total;
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/cartUser', $data);
        $this->view('user/galeri/footer');        
    }

    public function checkout(){        
        $totalHarga = $_POST['time'] * $_POST['harga'];
        $tgl_booking = strtotime($_POST['tanggal_booking']);
        $tgl_pinjam = strtotime($_POST['tanggal_pinjam']);
        $range = $tgl_pinjam - $tgl_booking;
        if ($range < 0) {
            Flasher::setCheckoutFlash('Gagal, tanggal tidak sesuai', 'danger');
            header('Location: ' . BASEURL . '/user_cart/verified');
            exit;
        }
        if($this->model('Cart_model')->addToCheckout($_POST, $totalHarga) > 0 ){
            $this->model('Cart_model')->deleteDataCart($_POST['id_user']);
            Flasher::setCheckoutFlash('Berhasil ditambahkan ke checkout', 'success');
            header('Location: ' . BASEURL . '/userHistory');
            exit;
            
        }else {
            Flasher::setCheckoutFlash('Gagal ditambahkan ke checkout');
            header('Location: ' . BASEURL . '/user_cart/verified');
            exit;
        }               
    }

}