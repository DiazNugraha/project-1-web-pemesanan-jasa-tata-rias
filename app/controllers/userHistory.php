<?php 


class userHistory extends Controller{
    public function index(){
        $data['judul'] = 'TORES WEB || History';
        $data['status_history'] = 'active';
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $data['product'] = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($data['product']);
        $data['user_check'] = $this->model('Checkout_model')->getAllCheckout();
        $parse = (integer)$data['product'];
        $total = 0;
        foreach ($data['product'] as $product) {
            $int = (integer)$product['hargaUpdate'];
            $total += $int;
        }
        $data['total'] = $total;

        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/history', $data);
        $this->view('user/galeri/footer');
    }

    public function detailData($id){
        $data['judul'] = 'TORES WEB || History';
        $data['status_history'] = 'active';
        $data['user'] = $this->model('User_model')->getUserById($_SESSION['user_id']);
        $data['product'] = $this->model('Cart_model')->getAllCart();
        $data['cart'] = count($data['product']);
        $data['user_check'] = $this->model('Checkout_model')->getAllCheckout();
        $data['detail'] = true;
        $data['select'] = $this->model('Checkout_model')->getCheckoutById($id);
        $data_baju = explode(", ", $data['select']['id_baju']);
        foreach($data_baju as $gmbr){
            $hasil[] = $this->model('Baju_model')->getBajuById($gmbr);            
        }
        $data['data_baju'] = $hasil;
        $data_jumlah = explode(", ", $data['select']['jumlah']);
        $data['data_jumlah'] = $data_jumlah;
        $this->view('user/galeri/header', $data);
        $this->view('user/galeri/detailHistory', $data);
        $this->view('user/galeri/footer');        
    }
}