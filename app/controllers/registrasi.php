<?php

class registrasi extends Controller{
    public function index(){
        $data['judul'] = 'TORES WEB || Registrasi';
        $this->view('registrasi/header', $data);
        $this->view('registrasi/index', $data);
        $this->view('registrasi/footer');
    }

    public function TambahUser(){
        
        if ($this->model('User_model')->tambahDataUser($_POST) > 0) {
            Flasher::setRegistrasiFlash('Berhasil', 'Tambah', 'green');
            header('Location:' . BASEURL . '/login');
            exit;
        }
        else {
            Flasher::setRegistrasiFlash('Gagal', 'Tambah', 'red');
            header('Location:' . BASEURL . '/login');
            exit;
        }

     
    }

}