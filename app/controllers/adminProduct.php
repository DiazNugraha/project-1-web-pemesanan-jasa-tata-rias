<?php 

class adminProduct extends Controller{
    public function index(){
        $data['judul'] = 'Products || Admin';
        $data['admin'] = $this->model('Admin_model')->getAdminDataById($_SESSION['admin_id']);
        $data['product'] = $this->model('Baju_model')->getAllBaju();
        $this->view('admin/header', $data);
        $this->view('admin/product/index', $data);        
        $this->view('admin/footer');
    }

    public function details($id){
        $data['judul'] = 'Products || Details || Admin';
        $data['admin'] = $this->model('Admin_model')->getAdminDataById($_SESSION['admin_id']);
        $data['product'] = $this->model('Baju_model')->getBajuById($id);
        $this->view('admin/header', $data);
        $this->view('admin/product/detail', $data);
        $this->view('admin/footer');
    }

    public function edit($id){
        $data['judul'] = 'Products || Edit || Admin';
        $data['admin'] = $this->model('Admin_model')->getAdminDataById($_SESSION['admin_id']);
        $data['product'] = $this->model('Baju_model')->getBajuById($id);
        $data['product'] = $this->model('Baju_model')->getBajuById($id);
        $this->view('admin/header', $data);
        $this->view('admin/product/edit', $data);
        $this->view('admin/footer');
    }

    public function editProduct($id){
        $data['product'] = $this->model('Baju_model')->getBajuById($id);        
        if ($_POST['harga'] == $data['product']['harga'] && $_POST['jumlah'] == $data['product']['jumlah']) {
            Flasher::setEditProductFlash('Gagal', 'Edit, Data masih sama', 'danger');
            exit;
        }
        if ( $this->model('Baju_model')->editDataBaju($id, $_POST) ) {
            Flasher::setEditProductFlash('Berhasil', 'Edit', 'success');
            header('Location: ' . BASEURL . '/adminProduct');
            exit;                        
        }else {
            Flasher::setEditProductFlash('Gagal', 'Edit', 'danger');
            header('Location: ' . BASEURL . '/adminProduct');
            exit;
        }         
        // $this->model('Baju_model')->editDataBaju($id, $_POST);
    }

    public function tambahProduct(){
        if ( $this->model('Baju_model')->tambahBaju($_POST) > 0 ) {
            Flasher::setTambahProductFlash('Berhasil', 'Tambah', 'success');
            header('Location: ' . BASEURL . '/adminProduct');
            exit;
        }else{
            Flasher::setTambahProductFlash('Gagal', 'Tambah', 'danger');
            header('Location: ' . BASEURL . '/adminProduct');
            exit;
        }        
    }

    public function deleteProduct($id){
        if ( $this->model('Baju_model')->deleteBaju($id) > 0) {
            Flasher::setDeleteProductFlash('Berhasil', 'Hapus', 'success');
            header('Location: ' . BASEURL . '/adminProduct');
            exit;
        }else {
            Flasher::setDeleteProductFlash('Gagal', 'Hapus', 'danger');
            header('Location: ' . BASEURL . '/adminProduct');
            exit;
        }
    }

}