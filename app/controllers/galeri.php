<?php 

class galeri extends Controller{
    public function index(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['status_galeri'] = 'active';
        $data['barang'] = $this->model('Baju_model')->getAllBaju();
        $data['makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();
        $data['jumlah_baju'] = count($data['barang']);
        $data['jumlah_tata_rias'] = count($data['makeUp']);
        $data['salon'] = $this->model('Salon_model')->getAllSalon();
        $data['header'] = 'All Product';
        $data['status'] = 'all';
        $data['styles'] = 'galeri.css';
        $this->view('galeri/header', $data);
        $this->view('galeri/index', $data);
        $this->view('galeri/footer');
    }

    // Tata Rias

    public function tata_rias(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['makeUp_state'] = 'active';        
        $data['makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();
        $data['barang'] = $this->model('Baju_model')->getAllBaju();
        $data['header'] = 'All Tata Rias';
        $data['jumlah_baju'] = count($data['barang']);
        $data['jumlah_tata_rias'] = count($data['makeUp']);
        $data['status'] = 'single_makeUp';
        $this->view('galeri/header', $data);
        $this->view('galeri/index', $data);
        $this->view('galeri/footer');
    }

    public function sortTRPengantin(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['makeUp_state'] = 'active';
        $data['status'] = 'single_makeUp';
        $data['makeUp'] = $this->model('Tata_rias_model')->getAllTRPengantin();
        $data['header'] = 'All Tata Rias Pengantin';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $this->view('galeri/header', $data);
        $this->view('galeri/index', $data);
        $this->view('galeri/footer');
    }

    public function sortTRKhitanan(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['makeUp_state'] = 'active';        
        $data['makeUp'] = $this->model('Tata_rias_model')->getAllTRKhitanan();        
        $data['status'] = 'single_makeUp';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $data['header'] = 'All Tata Rias Khitanan';
        $this->view('galeri/header', $data);
        $this->view('galeri/index', $data);
        $this->view('galeri/footer');   
    }

    // End of tata rias

    // Salon

    public function salon(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['salon_state'] = 'active';
        $data['status'] = 'single_salon';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $data['salon'] = $this->model('Salon_model')->getAllSalon();
        $data['header'] = 'All Salon';
        $this->view('galeri/header', $data);
        $this->view('galeri/index', $data);
        $this->view('galeri/footer');
    }

    // End of salon

    // Baju

    public function detailBaju($id){
        // var_dump($id);
        $data['judul'] = 'TORES WEB || Details Gambar';
        $data['baju_state'] = 'active';        
        $data['barang'] = $this->model('Baju_model')->getBajuById($id);
        $this->view('galeri/header', $data);
        $this->view('galeri/details', $data);
        $this->view('galeri/footer');
    }

    public function sortKebaya(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['baju_state'] = 'active';
        $data['status'] = 'single_baju';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $data['barang'] = $this->model('Baju_model')->getAllKebaya();
        $data['header'] = 'All Kebaya';
        $this->view('galeri/header', $data);
        $this->view('galeri/index', $data);
        $this->view('galeri/footer');
    }

    public function sortBajuJas(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['baju_state'] = 'active';
        $data['status'] = 'single_baju';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $data['barang'] = $this->model('Baju_model')->getAllBajuJas();
        $data['header'] = 'All Baju Jas';
        $this->view('galeri/header', $data);
        $this->view('galeri/index', $data);
        $this->view('galeri/footer');
    }

    public function sortBajuPengantin(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['baju_state'] = 'active';
        $data['status'] = 'single_baju';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $data['barang'] = $this->model('Baju_model')->getAllBajuPengantin();
        $data['header'] = 'All Baju Pengantin';
        $this->view('galeri/header', $data);
        $this->view('galeri/index', $data);
        $this->view('galeri/footer');
    }

    public function baju(){
        $data['judul'] = 'TORES WEB || Galeri';
        $data['baju_state'] = 'active';
        $data['status'] = 'all_baju';
        $data['jumlah_baju'] = $this->model('Baju_model')->getAllBaju();
        $data['jumlah_makeUp'] = $this->model('Tata_rias_model')->getAllTataRias();        
        $data['jumlah_baju'] = count($data['jumlah_baju']);
        $data['jumlah_tata_rias'] = count($data['jumlah_makeUp']);
        $data['barang'] = $this->model('Baju_model')->getAllBaju();
        $data['header'] = 'All Baju';
        $this->view('galeri/header', $data);
        $this->view('galeri/index', $data);
        $this->view('galeri/footer');
    }

    // End of baju
    
}


?>