<?php 


class adminDashboard extends Controller{
    public function index(){
        $data['judul'] = 'Dashboard Admin';
        $data['admin'] = $this->model('Admin_model')->getAdminDataById($_SESSION['admin_id']);        
        $data['customer'] = $this->model('Admin_model')->getAllProceededCustomer();
        $data['product'] = $this->model('Baju_model')->getAllBaju();
        $data['count_product'] = count($data['product']);            
        $data['pendapatan'] = $this->model('Admin_model')->getPenghasilan();
        $data['pendapatan'] = $data['pendapatan']['pendapatan'];
        $data['proceeded'] = count($data['customer']);
        $data['pending'] = $this->model('Admin_model')->getAllPendingCustomer();
        $parse = (integer)$data['pending'];
        $data['jumlah'] = count($data['pending']);                
        $this->view('admin/header', $data);
        $this->view('admin/dashboard', $data);
        $this->view('admin/footer');        
    }
}