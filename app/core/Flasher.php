<?php

class Flasher {
    public static function setFlash($pesan, $aksi, $tipe){
        $_SESSION['flash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];

    }

    public static function setCartFlash($pesan, $aksi, $tipe){
        $_SESSION['cartFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setLoginFlash($pesan, $aksi, $tipe){
        $_SESSION['loginFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setLoginAdminFlash($pesan, $aksi, $tipe){
        $_SESSION['loginAdminFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setRegistrasiFlash($pesan, $aksi, $tipe){
        $_SESSION['registrasiFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setTambahCart($pesan, $aksi, $tipe){
        $_SESSION['tambahCart'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setCheckoutFlash($pesan, $tipe){
        $_SESSION['checkoutFlash'] = [
            'pesan' => $pesan,
            'tipe' => $tipe
        ];
    }

    public static function setProceedFlash($pesan, $aksi, $tipe){
        $_SESSION['proceedFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setDeleteUserFlash($pesan, $aksi, $tipe){
        $_SESSION['deleteUserFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setEditProductFlash($pesan, $aksi, $tipe){
        $_SESSION['editProductFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setDeleteCustomerFlash($pesan, $aksi, $tipe){
        $_SESSION['deleteCustomerFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setTambahProductFlash($pesan, $aksi, $tipe){
        $_SESSION['tambahProductFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setDeleteProductFlash($pesan, $aksi, $tipe){
        $_SESSION['deleteProductFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }


    public static function flash(){
        if ( isset($_SESSION['flash']) ) {
            echo '<div class="alert alert-' . $_SESSION['flash']['tipe'] . ' alert-dismissible fade show" role="alert">
            Data Mahasiswa <strong>' . $_SESSION['flash']['pesan'] . '</strong> ' . $_SESSION['flash']['aksi'] . '
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';            
            unset($_SESSION['flash']);
        }
        
    }

    public static function loginFlash(){
        if (isset($_SESSION['loginFlash'])) {
            echo '<i style="color: ' . $_SESSION['loginFlash']['tipe'] . '; font-style: italic;">User ' . $_SESSION['loginFlash']['pesan'] . ' ' . $_SESSION['loginFlash']['aksi'] . '</i>';
            unset($_SESSION['loginFlash']);
        }
    }

    public static function loginAdminFlash(){
        if (isset($_SESSION['loginAdminFlash'])) {
            echo '<i style="color: ' . $_SESSION['loginAdminFlash']['tipe'] . '; font-style: italic;">Admin ' . $_SESSION['loginAdminFlash']['pesan'] . ' ' . $_SESSION['loginAdminFlash']['aksi'] . '</i>';
            unset($_SESSION['loginAdminFlash']);
        }
    }

    public static function registrasiFlash(){
        if (isset($_SESSION['registrasiFlash'])) {
            echo '<i style="color: ' . $_SESSION['registrasiFlash']['tipe'] . '; font-style: italic;">' . $_SESSION['registrasiFlash']['pesan'] . ' ' . $_SESSION['registrasiFlash']['aksi'] . ' Data User</i>';
            unset($_SESSION['registrasiFlash']);
        }
    }

    public static function cartFlash(){
        if (isset($_SESSION['cartFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['cartFlash']['tipe'] . ' alert-dismissible fade show" role="alert">
            <strong>' . $_SESSION['cartFlash']['pesan'] . '</strong> ' . $_SESSION['cartFlash']['aksi'] . ' data
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';            
            unset($_SESSION['cartFlash']);
        }
    }

    public static function tambahCart(){
        if(isset($_SESSION['tambahCart'])){
            echo '<div class="alert alert-' . $_SESSION['tambahCart']['tipe'] . ' alert-dismissible fade show" role="alert">
            <strong>' . $_SESSION['tambahCart']['pesan'] . '</strong> ' . $_SESSION['tambahCart']['aksi'] . ' cart
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
            unset($_SESSION['tambahCart']);
        }
    }

    public static function checkoutFlash(){
        if (isset($_SESSION['checkoutFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['checkoutFlash']['tipe'] . ' alert-dismissible fade show" role="alert">
            <strong>' . $_SESSION['checkoutFlash']['pesan'] . '
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
            unset($_SESSION['checkoutFlash']);
        }
    }

    public static function proceedFlash(){
        if (isset($_SESSION['proceedFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['proceedFlash']['tipe'] . ' alert-dismissible fade show" role="alert">
            <strong>' . $_SESSION['proceedFlash']['pesan'] . '</strong> ' . $_SESSION['proceedFlash']['aksi'] . ' request
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
            unset($_SESSION['proceedFlash']);
        }
    }

    public static function deleteUserFlash(){
        if (isset($_SESSION['deleteUserFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['deleteUserFlash']['tipe'] . ' alert-dismissible fade show" role="alert">
            <strong>' . $_SESSION['deleteUserFlash']['pesan'] . '</strong> ' . $_SESSION['deleteUserFlash']['aksi'] . ' User
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
            unset($_SESSION['deleteUserFlash']);
        }
    }

    public static function editProductFlash(){
        if (isset($_SESSION['editProductFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['editProductFlash']['tipe'] . ' alert-dismissible fade show" role="alert">
            <strong>' . $_SESSION['editProductFlash']['pesan'] . '</strong> ' . $_SESSION['editProductFlash']['aksi'] . ' Product
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
            unset($_SESSION['editProductFlash']);
        }
    }

    public static function deleteCustomerFlash(){
        if (isset($_SESSION['deleteCustomerFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['deleteCustomerFlash']['tipe'] . ' alert-dismissible fade show" role="alert">
            <strong>' . $_SESSION['deleteCustomerFlash']['pesan'] . '</strong> ' . $_SESSION['deleteCustomerFlash']['aksi'] . ' Customer
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
            unset($_SESSION['deleteCustomerFlash']);
        }
    }

    public static function tambahProductFlash(){
        if (isset($_SESSION['tambahProductFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['tambahProductFlash']['tipe'] . ' alert-dismissible fade show" role="alert">
            <strong>' . $_SESSION['tambahProductFlash']['pesan'] . '</strong> ' . $_SESSION['tambahProductFlash']['aksi'] . ' Product
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
            unset($_SESSION['tambahProductFlash']);
        }
    }

    public static function deleteProductFlash(){
        if (isset($_SESSION['deleteProductFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['deleteProductFlash']['tipe'] . ' alert-dismissible fade show" role="alert">
            <strong>' . $_SESSION['deleteProductFlash']['pesan'] . '</strong> ' . $_SESSION['deleteProductFlash']['aksi'] . ' Product
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>';
            unset($_SESSION['deleteProductFlash']);
        }
    }


}